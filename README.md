# Compilateur C

Un compilateur d'un sous ensemble de C

## Sommaire

- [Compilateur C](#compilateur-c)
  - [Sommaire](#sommaire)
  - [Systèmes d'exploitation compatibles](#syst%c3%a8mes-dexploitation-compatibles)
  - [Prérequis](#pr%c3%a9requis)
  - [Image Docker](#image-docker)
    - [Ouvrir le projet dans un container Docker](#ouvrir-le-projet-dans-un-container-docker)
  - [Build le projet](#build-le-projet)
    - [Build le projet avec docker](#build-le-projet-avec-docker)
  - [Éxecuter le projet](#%c3%89xecuter-le-projet)
    - [Utilisation de `c-compiler`](#utilisation-de-c-compiler)
    - [Utilisation de `tests`](#utilisation-de-tests)
    - [Tests systèmes](#tests-syst%c3%a8mes)
      - [Exécuter les tests avec docker](#ex%c3%a9cuter-les-tests-avec-docker)
  - [Fonctionnalités](#fonctionnalit%c3%a9s)
    - [Fonctionnalités supportées:](#fonctionnalit%c3%a9s-support%c3%a9es)
    - [AST](#ast)
    - [Génération du graphe de l’AST sous forme d’image avec GraphViz](#g%c3%a9n%c3%a9ration-du-graphe-de-last-sous-forme-dimage-avec-graphviz)
    - [Les variables en mémoire](#les-variables-en-m%c3%a9moire)
  - [Structure du code](#structure-du-code)
  - [Gestion du projet](#gestion-du-projet)
    - [Les adaptations au confinement](#les-adaptations-au-confinement)
    - [Gestion de projet à venir](#gestion-de-projet-%c3%a0-venir)
    - [Erreurs connues](#erreurs-connues)

## Systèmes d'exploitation compatibles

Fonctionnel sur :

- [Ubuntu](https://www.ubuntu.com/)
- [Archlinux](https://www.archlinux.org/)
- [ElementaryOS](https://elementary.io/)

Testé sur :

- MacOS

Non testé sur :

- Windows

## Prérequis

- Compilateur compatible avec [C++ 17](https://en.cppreference.com/w/cpp/17)
- [Boost 1.72](https://www.boost.org/users/history/version_1_72_0.html)
- [antlr4](https://github.com/antlr/antlr4/) (fourni par le projet)
- [CMake 1.10](https://github.com/Kitware/CMake/releases/tag/v3.10.3)
- makefile

## Image Docker

Pour simplifier l’installation des prérequis, une image docker a été mise en place à partir de l’image de M. Guerin. Elle comprend déjà tous les paquets nécessaires.

L’image est disponible sur [Docker Hub](https://hub.docker.com/r/hugnata/pldcomp).

### Ouvrir le projet dans un container Docker

Si vous souhaitez ouvrir le projet directement via un container docker, vous pouvez exécuter :

```shell script
sh ./scripts/docker_bash.sh
```

_Note : Une fois dans le container docker, faire `cd /work` pour accéder au dossier du projet_

## Build le projet

```bash
git clone --depth=1 https://gitlab.com/htpps/compilateur-c
cd compilateur-c
mkdir cmake-build-debug && cd cmake-build-debug
cmake -G "Unix Makefiles" ..
make all
```

### Build le projet avec docker

Pour build le projet avec le container docker que nous fournissons, vous pouvez exécuter :

```shell script
sh ./scripts/compile_docker.sh
```

## Éxecuter le projet

Les exécutables sont générés dans le dossier `cmake-build-debug/bin`

- `c-compiler` est l'exécutable du compilateur
- `tests` est l'exécutable des tests unitaires

### Utilisation de `c-compiler`

Exécuter `c-compiler --help` affiche les options et les paramètres attendus par l'exécutable.

Usage : `c-compiler [options] <fichier source>`

| Option                | Description                                                                                                                                                                                         |
| --------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `-h`, `--help`        | affiche le message d'aide                                                                                                                                                                           |
| `-v`, `--verbose`     | affiche les messages de débuggage                                                                                                                                                                   |
| `-o`, `--output-file` |  le nom du fichier qui contiendra l'assembleur (`a.s` par défaut)                                                                                                                                   |
| `--ast`               | génère un fichier .dot (utilisable avec [Graphviz](https://graphviz.org/)) qui représente l'AST généré lors de la compilation (il est possible de préciser un nom de fichier, `ast.dot` par défaut) |

### Utilisation de `tests`

L'exécutable `tests` est généré avec [Boost.Test](https://www.boost.org/doc/libs/1_72_0/libs/test/doc/html/index.html).

Exécuter `tests --help` affiche toutes les options possibles.

Voici quelques exemples :

- `tests` : exécute tous les tests
- `tests --list_content` : affiche tous les ensembles de tests et les tests qu'ils contiennent
- `tests --run_test=<ensemble de tests>` : exécute uniquement l'ensemble de tests spécifié
- `tests --run_test=<ensemble de tests>/<test>` : exécute uniquement le test spécifié

### Tests systèmes

Les tests du compilateur compilent tout un ensemble de fichier C avec `gcc` puis avec notre compilateur et compare les résultats en sortie.
Si les deux compilateurs ne produisent pas le même résultat (compilation ou non compilation du code C), ou que l'éxécution du code compilé ne produit pas la même sortie, le test est alors faux.

Pour éxécuter tous les tests, il suffit de faire

```bash
cd tests/
./test_if.sh
```

#### Exécuter les tests avec docker

```shell script
sh ./tests/test.sh
```

## Fonctionnalités

### Fonctionnalités supportées:

Nous supportons:

- constantes entières, variables de type int
- définition de fonctions avec 6 paramètres maximum et pour type de retour `int`
- la déclaration de variables (notamment multiligne): `int a,b,c;`
- l'affectation de variables: `int a = 2;`,`a = 3;`, `int a = b = c;`
- Structure de blocs grâce à { et }
- Structures de contrôle (if, else, while, for, do...while)
- Les expressions arithmétiques (addition, soustraction, multiplication, division, modulo, combinaison d'opérations et parenthèses)
- opérations de comparaison (==, !=, <, <=, >, >=)
- opérations logiques paresseux (||, &&)
- opération unaire (-)
- les commentaires
- Uniquement le type `int`

Nous avons un _undefined behaviour_ lorsque il y a un `return` avant la fin de la fonction. L'exemple suivant renvoie 0 :

```c
int main() {
    {
        return 1;
    }
        return 0;
}
```

### AST

Nous avons fait le choix de créer un AST afin de nous faciliter la tâche pour la suite du processus de compilation. Cet AST est un ensemble de classes dont les relations suivent de manière logique la grammaire que nous avons créée. Après lecture du fichier C par antlr, le parcours de notre implémentation des visiteurs permet de créer un AST représentatif du code lu. Ensuite, le parcours de cet AST permet de remplir la table des symboles et de générer l’assembleur. On pourra par la suite imaginer des opérations d’optimisation sur l’AST.

Voila à titre d'exemple l'AST généré pour le code suivant:

```c
int hcf(int na, int nb) {
    if (nb != 0)
        return hcf(nb, na % nb);
    else
        return na;
}

int main() {
    int na = 420, nb = 429;
    return hcf(na, nb);
}
```

![Image](https://media.discordapp.net/attachments/681857499112275998/697384249124454450/ast.png)

### Génération du graphe de l’AST sous forme d’image avec GraphViz

Il est possible de générer une image du graphe de l’AST représentant l’ensemble des relations entre les expressions et instructions de notre programme. Chaque noeud représente les éléments du code source passés à notre compilateur et permet de vérifier que la grammaire est correcte.
Le graphe généré est sous format DOT.
Pour visualiser le graphe comme une image svg ou png, il faut exécuter une des commandes suivantes :

```bash
dot -Tsvg graphe.dot -o graphe.svg
dot -Tpng graphe.dot -o graphe.png
```

### Les variables en mémoire

Notre compilateur permet d’analyser et compiler du code contenant une fonction main ne prenant aucun paramètre et retournant soit une variable, soit une constante. Il est possible de déclarer plusieurs variables sur une même ligne, et d’affecter une valeur contenue dans une constante ou une variable à plusieurs variables sur une même ligne.
Il est aussi possible de le faire sur des lignes distinctes.

Une table des symboles a été mise en place pour assurer une certaine cohérence :
pour utiliser une variable, elle doit être déclarée,
deux variables ne peuvent pas avoir le même nom dans le même bloc (même si pour l’instant il n’est pas possible d’avoir un autre bloc que la fonction principale main).

La table des symboles s’occupe de l’attribution d’une adresse mémoire lorsqu’une variable est ajoutée à la table des symboles.

---

Une fonctionnalité de la table des symboles permet de vérifier que toutes les variables déclarées sont bien utilisées. Si ce n'est pas le cas, un avertissement sera donné par le compilateur, précisant les variables non utilisées.

De plus, une étape supplémentaire (réalisée par la classe VariableChecker) permet de :

- faire le lien entre l'AST, la table des symboles et la table des fonctions,
- vérifier les exceptions qui ont été levées,
- vérifier que la variable lors de l'affectation est à droite. /todo

### IR

L'IR est une représentation intermédiaire du code. Il permet d'ajouter une étape supplémentaire entre l'AST et la génération du code cible (x86 dans notre cas). 

Il est composé des classes :
- IRInstr
- BasicBlock
- CFG (Control Flow Graph)

## Structure du code

Le code de l'application est séparé en 3 parties :

- la librairie `compiler` qui contient le cœur du compilateur (AST, table des symboles, etc)
- l'exécutable principal `c-compiler` qui sert de CLI à la librairie
- l'exécutable de tests `tests` qui contient les tests unitaires

Le code du compilateur a été mis à part dans une librairie pour faciliter son intégration dans 2 exécutables différents.

Les dossiers sont organisés comme suit :

- `antlr4-generated` ✅ : contient les fichiers générés par antlr
- [`cmake`](cmake) : contient les scripts cmake nécessaires pour intégrer antlr au projet
- `cmake-build-debug` ✅ : contient tous les fichiers générés par CMake et makefile
- [`scripts`](scripts) : contient quelques scripts bash
- [`g4`](g4) : contient les fichiers de grammaire antlr
- [`include`](include) : contient tous les headers du projet
  - [`lib`](include/lib) : contient les headers de la librairie
  - [`main`](include/main) : contient les headers de l'exécutable principal
- [`lib`](lib) : contient le `jar` du générateur de code d'antlr
- [`src`](src) : contient toutes les sources du projet
  - [`lib`](src/lib) : contient les sources de la librairie
  - [`main`](src/main) : contient les sources de l'exécutable principal
  - [`tests`](src/tests) : contient les sources de l'exécutable de tests
- [`tests`](tests) : contient les tests du compilateur
  - [`tests`](tests) : contient l'ensemble des fichier C éxécuté lors des tests
  - [`not_implemented_yet`](tests/not_implemented_yet) : contient des fichiers C de tests qui seront ajouté par la suite

✅ : indique les dossiers générés lors du build et donc absents du repo git

## Gestion du projet

Nous avons mis en place un système de création d’issues pour pouvoir gérer notre projet d’une façon qui se rapproche du SCRUM. À chaque fonctionnalité est liée une issue Gitlab.
Nous exploitons le système de gestion de projet intégré à Gitlab (possibilité d’assigner des tâches, kanban, …). Cela nous a permis d’être à jour dans le travail qu’il restait à réaliser.
Ainsi à chaque fois qu’une nouvelle fonctionnalité doit être implémentée, une nouvelle branche est créée avec le nom de l’issue Gitlab, pour lier les deux.

Une fois que la fonctionnalité est implémentée et que les tests fonctionnels passent, il faut fusionner la branche avec la principale. Pour cela, une merge request est créée par Gitlab et nous en sommes informé via notre serveur Discord, dans un salon dédié, par un message automatique du type “PaulGOUX27 (PaulGOUX27) merged !branch_name in htpps / Compilateur C”.
Pour que la fonctionnalité soit ajoutée, il faut la validation de deux personnes, qui relisent et commentent le code. Cela permet deux choses : de détecter certaines erreurs ou oublis et de rester au courant des dernières modifications.

En plus de cette procédure de pair-reviewing, nous avons mis en place de l’intégration continue. En effet chaque commit doit passer trois étapes:
L’étape de build, qui compile le programme et construit les exécutables,
L’étape de tests unitaires, qui est effectuée grâce à la librairie de Boost.Test,
L’étape de tests d’intégration, qui est effectuée avec le framework de tests fourni.

Si une de ces étapes ne réussit pas, nous en sommes informés et la fonctionnalité ne peut pas être fusionnée à la branche master.

Des réunions régulières sont organisées (en début de séance, en milieu de séance, et en dehors des cours).

### Les adaptations au confinement

Nous nous réunissons sur Discord pour pouvoir réaliser les réunions mais aussi travailler ensemble. Il est possible de se séparer sur plusieurs channels vocaux et simuler un pair-programming via le partage d’écran.

### Gestion de projet à venir

Dans le futur proche, nous prévoyons d’implémenter dans un premier temps un IR, puis d’implémenter les opérateurs arithmétiques. Des tests sont déjà prévus concernant les prochaines étapes à venir notamment pour tester les opérateurs arithmétiques, ce qui nous permettra de tester au fur et à mesure les fonctionnalités implémentées.

### Erreurs connues

La liste des erreurs connu est [ici](NotSupported.md)
