grammar ifcc;

// Axiome
axiom : prog       
      ;

// Non terminaux
/** Le programme.

Constitué de int main() {} pour l'instant
*/
prog : functions+=func* 'int' 'main' '(' ')' bloc;

func : TYPE VAR '(' (parameter (',' parameter)*)? ')' bloc;

/**
Un bloc d'instructions
*/
bloc : '{' instructions+=inst* '}';

/** Une instruction

Peut être:

- un expression -> a=2;
- une déclaration -> int a;
- une instruction return -> return a;
*/


inst : expr ';' # instExpr
    | bloc # instBloc
    | TYPE decl (',' decl)* ';' # instDecl
    | RETURN expr ';' # instReturn
    |
      'if' '(' predicate = expr ')' ifInst = inst
      ('else' elseInst = inst)? # if
    | 'while' '(' predicate = expr ')' inst # while
    | 'for' '('
        (initExpr = expr | TYPE initDecl = decl)? ';'
        condition = expr? ';'
        iteration = expr?
      ')' inst # for
    | 'do' inst 'while' '(' predicate = expr ')' ';' # doWhile
    ;

/** Une expression

Peut être

- une constante -> 3
- Une variable -> a
- Priorité sur les parenthèses -> (3 + 4) * 8
- Une affectation -> a = 3
- Expression multiplicative -> 4 / 8, 5 * 6, 4 % 7
- Expression additive -> 4 + 78, 7 - 8
*/
expr : VAR '(' (expr (',' expr)*)? ')' # exprCall
    | CONST # exprConst
	| VAR # exprVar
	| '(' expr ')' # exprPar
	| affect # exprAffect
	| '-' expr # exprNeg
	| lExpr = expr op = ('*'|'/'|'%') rExpr = expr # exprMul
	| lExpr = expr op = ('+'|'-') rExpr = expr # exprAdd
	| lExpr = expr op = ('<'|'<='|'>'|'>=') rExpr = expr # exprCmp
	| lExpr = expr op = ('=='|'!=') rExpr = expr # exprEq
	| lExpr = expr '&&' rExpr = expr # exprAnd
	| lExpr = expr '||' rExpr = expr # exprOr
	;

/** Une déclaration

Peut être

- Une variable -> a
- Une affectation -> a = 3
*/
decl : VAR # declVar
	| affect # declAffect
	;

/** Une affectation

 ex: a = 3, a= b =3, ...

*/
affect : VAR '=' expr;

parameter: TYPE VAR;

//--- LEXER ---
// Terminaux - mot clés
TYPE: 'int';
RETURN : 'return' ;

// Terminaux
CONST : [0-9]+ ;
COMMENT : '/*' .*? '*/' -> skip ;
COMMENTLINE : '//' .*?  '\n' -> skip;
DIRECTIVE : '#' .*? '\n' -> skip ;
WS    : [ \t\r\n] -> channel(HIDDEN);
VAR : ([a-z]|[A-Z]|'_')([a-z]|[A-Z]|[1-9]|'_')*;

