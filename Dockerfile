FROM eguerin/antlr4cpp # On prend l'image de guerin

RUN apt-get update && apt-get install -y\ # On installe les dépendances
  cmake\
  wget\
  git\
	pkg-config\
	uuid-dev



RUN wget "https://dl.bintray.com/boostorg/release/1.72.0/source/boost_1_72_0.tar.bz2" # On dl ce FDP de boost

RUN tar --bzip2 -xf boost_1_72_0.tar.bz2 # décompression

WORKDIR "boost_1_72_0"

RUN ./bootstrap.sh --without-libraries=python # Installation

RUN ./b2 install
