# Liste des cas non supporté

## Affectation sans parenthèse dans une expression

```C
int main(){
    int a;
    return (3+4)*a = 8-7/8%4;
}
```

- gcc
  ```
  test.c: In function ‘main’:
  test.c:3:17: error: lvalue required as left operand of assignment
    return (3+4)*a = 8-7/8%4;
  ```
- Notre compilateur : valide la syntaxe, `a` vaut `8-7/8%4`.

## Signe avant une variable

```C
int main(){
    int a = 3;
    int b = 7;
    return +b+a;
}
```

```C
int main(){
    int a = 3;
    int b = 7;
    return -b+a;
}
```

- code valide pour gcc mais pas pour notre grammaire

## Opérateur `--`

L'opérateur `--` n'est pas implémenté.
Ainsi une expression comme `a -- b` est interprétée comme `a - - b` ou `a - (-b)`.

