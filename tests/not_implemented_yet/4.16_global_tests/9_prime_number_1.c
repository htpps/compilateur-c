//
// Created by matmont98 on 06/04/2020.
//


int checkprime(int n) {
    int i=2, isprime = 1;

    while (i <= n / 2 && isprime != 0){
        if (n % i == 0) {
            isprime = 0;
        }
        i = i + 1;
    }

    return isprime;
}

int main(){
    int nb = 16;
    return checkprime(nb);
}