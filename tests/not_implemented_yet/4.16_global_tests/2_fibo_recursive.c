//
// Created by matmont98 on 06/04/2020.
//

int fibonacci(int n)
{
    if((n==1)||(n==0))
    {
        return(n);
    }
    else
    {
        return(fibonacci(n-1)+fibonacci(n-2));
    }
}

int main()
{
    int n = 10,i=0, finalterm;
    while(i<n)
    {
        finalterm = fibonacci(i);
        i = i + 1;
    }

    return finalterm;
}