//
// Created by matmont98 on 06/04/2020.
//
int func(int a){
    {
        int a = 3;
        {
            int a = 4;
            {
                int a = 5;
            }
        }
    }
    return a;
}

int main() {
    int a = 2;
    {
        int a = 3;
        {
            int a = 4;
            {
                int a = 5;
            }
        }
    }

    return func(a);
}
