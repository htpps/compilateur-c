//
// Created by matmont98 on 06/04/2020.
//

int funcc(int a){
    a = 15;
    {
        int a = 16;
        {
            int a = 17;
            {
                int a = 18;
            }
        }
    }
    return a;
}


int funcb(int a){
    a = 11;
    {
        int a = 12;
        {
            int a = 13;
            {
                int a = 14;
            }
        }
    }
    return funcc(a);
}


int funca(int a){
    a = 7;
    {
        int a = 8;
        {
            int a = 9;
            {
                int a = 10;
            }
        }
    }
    return funcb(a);
}

int main() {
    int a = 2;
    {
        int a = 3;
        {
            int a = 4;
            {
                int a = 5;
            }
        }
    }

    return funca(a);
}
