//
// Created by matmont98 on 06/04/2020.
//
int hcf(int na, int nb) {
    if (nb != 0)
        return hcf(nb, na % nb);
    else
        return na;
}

int main() {
    int na = 420, nb = 429;
    return hcf(na, nb);
}

