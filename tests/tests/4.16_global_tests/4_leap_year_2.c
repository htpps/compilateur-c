//
// Created by matmont98 on 06/04/2020.
//

int main() {
    int year=2021;
    int isleapyear;

    if (year % 4 == 0) {
        if (year % 100 == 0) {
            // the year is a leap year if it is divisible by 400.
            if (year % 400 == 0)
                isleapyear = 1;
            else
                isleapyear = 0;
        } else
            isleapyear = 1;
    } else
        isleapyear = 0;

    return isleapyear;
}

