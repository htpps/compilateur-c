//
// Created by matmont98 on 06/04/2020.
//

int main() {
    int i, n = 8, ta = 0, tb = 1, nextterm;

    for (i = 1; i <= n; i = i +1) {
        nextterm = ta + tb;
        ta = tb;
        tb = nextterm;
    }

    return nextterm;
}