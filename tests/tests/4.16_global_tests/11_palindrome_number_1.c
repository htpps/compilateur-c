//
// Created by matmont98 on 07/04/2020.
//

int main() {
    int n = 4554, reversedN = 0, remainder, originalN, res;
    originalN = n;

    while (n != 0) {
        remainder = n % 10;
        reversedN = reversedN * 10 + remainder;
        n = n / 10;
    }

    if (originalN == reversedN)
        res = 1;
    else
        res = 0;

    return res;
}