//
// Created by linux on 05/04/2020.
//

#include <Logging.h>
#include "VariableChecker.h"

using namespace std;

VariableChecker::VariableChecker(SymbolTable *symbolTable, FunctionTable *functionTable){
    this->symbolTable = symbolTable;
    this->functionTable = functionTable;
    this->onTheRight = false;
}

void VariableChecker::setOnTheRight(bool _onTheRight){
    this->onTheRight = _onTheRight;
}

void VariableChecker::openContext(const std::string& name){
    symbolTable->openContext(name);
}

void VariableChecker::closeContext(){
    symbolTable->closeContext();
}


void VariableChecker::addSymbolToTable(Symbol* symbol){
    try{
        symbolTable->addSymbol(symbol);
    }
    catch(GenericException &e){
        exceptions.push_back(e);
    }
}

void VariableChecker::setSymbolInTableUsed(const string& name){
    try{
        symbolTable->setSymbolInTableUsed(name);
    }
    catch(GenericException &e){
        exceptions.push_back(e);
    }
}

void VariableChecker::setSymbolInit(const string& name) {
    try{
        symbolTable->setSymbolInit(name);
    }
    catch(GenericException &e){
        exceptions.push_back(e);
    }
}

std::list<GenericException> VariableChecker::getExceptions(){
    return exceptions;
}

bool VariableChecker::isOnTheRight(){
    return onTheRight;
}

void VariableChecker::addFunctionToTable(Function *function) {
    try{
        functionTable->addFunction(function);
    }
    catch(GenericException &e){
        exceptions.push_back(e);
    }
}

void VariableChecker::doesFunctionExist(const string &name, int nbParams) {
    try{
        functionTable->doesFunctionExist(name, nbParams);
    }
    catch(GenericException &e){
        exceptions.push_back(e);
    }
}
