//
// Created by soplump on 18/03/2020.
//

#include <SymbolTable.h>

#include "SymbolTable.h"
#include "Exceptions.h"
#include "Logging.h"
#include <string_view>

using namespace std;

SymbolTable::SymbolTable() {
    //memoryAddress = BASE_ADDRESS;
}

void SymbolTable::addSymbol(Symbol* symbol) {
    LOG_TRACE << "SymbolTable - Adding "  << symbol->getName();

    std::string name = vectorToString(context) + symbol->getName();

    if(!symbol_table.count(name)) { // Check that the Symbol does not exist yet.

        // Set the address.
        if(MapTypeSize.count(symbol->getType())) {

            if(memoryAddress.count(context[0])==0){

                memoryAddress.insert({context[0],0});
            }
            memoryAddress[context[0]] += MapTypeSize.find(symbol->getType())->second;
            symbol->setAddress(memoryAddress[context[0]]);
        }
        else{
            throw GenericException(ExceptionType::UnsupportedTypeException, "Tried to declare a variable with an unsupported type (variable : " + symbol->getName() + ", type : " + Symbol::SymbolTypeToString(symbol->getType())+ ")");
        }

        // Insertion.
        symbol_table.insert({name, symbol});
    } else {
        throw GenericException(ExceptionType::AlreadyExistingSymbolException, "redeclaration of '" + symbol->getName() + "'");
    }
}

Symbol* SymbolTable::getSymbol(const std::string& var) {
    try{
        std::string name = findNameInTable(var);
        return symbol_table.find(name)->second;
    }
    catch(GenericException &e){
        throw e;
    }
}

void SymbolTable::setSymbolInTableUsed(const std::string& var){
    try{
        std::string name = findNameInTable(var);
        Symbol* symbol = symbol_table.find(name)->second;

        if(symbol->isInitialized())
            symbol->setUsed(true);
        else
            throw GenericException(ExceptionType::UninitializedVariableException, "'" + symbol->getName() +  "' is used uninitialized in this function");
    }
    catch(GenericException &e){
        throw e;
    }
}


void SymbolTable::setSymbolInit(const std::string& var){
    try{
        std::string name = findNameInTable(var);
        symbol_table.find(name)->second->setInitialized(true);
    }
    catch(GenericException &e){
        throw e;
    }
}

void SymbolTable::openContext(const std::string& ctx) {
    LOG_TRACE << "Symbol table - open context " << ctx;
    context.push_back(ctx);
}


void SymbolTable::closeContext() {
    if(!context.empty()){
        LOG_TRACE << "Symbol table - close context " << context.back();
        context.pop_back();
    }
    else{
        throw GenericException(ExceptionType::NoContextException, "Tried to close an empty context.");
    }
}

std::vector<Symbol *> SymbolTable::getUnusedSymbol() {
    auto res = std::vector<Symbol *>();

    for(const auto& it : symbol_table){              // Iterate over the vector symbol used.
        if(! it.second->isUsed()){                 // If the symbol is not used,
            res.push_back(symbol_table[it.first]);  // add the Symbol to the vector.
        }
    }

    return res;
}

// Returns the string representation of the vector of contexts.
std::string SymbolTable::vectorToString(const std::vector<std::string>& ctx) {
    std::string res = SEPARATOR;

    for(const auto& it : ctx) {
        res += it + SEPARATOR;
    }

    return res;
}

std::string SymbolTable::findNameInTable(const std::string& var){
    // Search in the current context, then its parent, then its parent etc.
    std::vector<std::string> ctx(context);

    LOG_TRACE << "Symbol table - finding " << var;

    do {
        LOG_TRACE << "Symbol table - current context " << vectorToString(ctx);

        std::string name = vectorToString(ctx) + var;

        if(symbol_table.count(name)) {
            LOG_TRACE << "Symbol table - found " << name;
            return name;
        }

        if(!ctx.empty()) {
            ctx.pop_back();
        }

    } while(!ctx.empty());

    dump();
    throw GenericException(ExceptionType::SymbolNotInTableException, "The symbol " + var + " is not in the symbol table." + "context: " + vectorToString(context));
}

void SymbolTable::dump() {
    LOG_TRACE << "Context : " << vectorToString(context);
    for (auto &it : symbol_table) {
        LOG_TRACE << it.first << " " << it.second->getName();
    }
}

int SymbolTable::getMemorySpaceInContext(std::string ctx) {
    int number = 0;
    for(auto& it : symbol_table) {
        if((it.first).rfind(ctx,0) == 0){
            number+= MapTypeSize.find(it.second->getType())->second;
        }
    }
    return number;
}

void SymbolTable::setContext(vector<string> ctx) {
    context = ctx;
}
