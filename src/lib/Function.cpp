//
// Created by linux on 07/04/2020.
//

#include "Function.h"

#include <utility>
#include <Exceptions.h>

using namespace std;

Function::Function(SymbolType typeRet, std::vector<SymbolType> typeParams, const std::string &name) : typeRet(
        typeRet), typeParams(std::move(typeParams)), name(name) {}

bool Function::operator==(const Function &rhs) const {
    if(typeParams.size() == rhs.typeParams.size()){
        bool res = typeRet == rhs.typeRet && name == rhs.name;

        for(int i = 0; i < typeParams.size(); ++i){
            res =  res && (typeParams[i] == rhs.typeParams[i]);
        }

        return  res;
    }
    else{
        return false;
    }
}

const std::string &Function::getName() const {
    return name;
}

int Function::getNbParams() const {
    return typeParams.size();
}

SymbolType Function::getParam(int indice) {
    if(indice < typeParams.size() ){
        return typeParams[indice];
    }
    else{
        throw GenericException(ExceptionTypeEnum::IndexParamFunctionOutOfRangeException, "Tried to access the index " + to_string(indice) + " of the function " + name + " that has " + to_string(getNbParams())+ " parameters." );
    }
}
