#include "Visitor.h"
#include "ast/ASTBooleanOperators.h"

using namespace std;

/**** Axiom ****/
antlrcpp::Any Visitor::visitAxiom(ifccParser::AxiomContext *ctx) {
    LOG_TRACE << "Axiom found" << endl;
    //TODO do something ou supprimer
    // return ifccBaseVisitor::visitAxiom(ctx);
    return visitProg(ctx->prog());
}

antlrcpp::Any Visitor::visitProg(ifccParser::ProgContext *ctx) {
    LOG_TRACE << "Prog found" << endl;
    auto *astProg = new ASTProg();

    for (auto function : ctx->functions) {
        astProg->addFunction(visit(function));
    }

    astProg->setBloc((ASTBloc *) visitBloc(ctx->bloc()));
    return (ASTProg *) astProg;
}

antlrcpp::Any Visitor::visitBloc(ifccParser::BlocContext *ctx) {
    LOG_TRACE << "Bloc found" << endl;
    ASTBloc *astBloc = new ASTBloc();
    for (auto instruction : ctx->instructions) {
        astBloc->addInstruction(visit(instruction));
    }
    return (ASTBloc *) astBloc;
}

/**  Instructions **/

antlrcpp::Any Visitor::visitInstExpr(ifccParser::InstExprContext *ctx) {
    LOG_TRACE << "Instruction Expression found" << endl;
    return (ASTInstr *) new ASTInstrExpr((ASTExpr *) visit(ctx->expr()));
}

antlrcpp::Any Visitor::visitInstDecl(ifccParser::InstDeclContext *ctx) {
    LOG_TRACE << "Instruction Declaration found" << endl;
    ASTInstrDecl *astInstrDecl = new ASTInstrDecl();
    for (auto decl:ctx->decl()) {
        astInstrDecl->addAstDeclaration(visit(decl));
    }


    return (ASTInstr *) astInstrDecl;
}

antlrcpp::Any Visitor::visitInstReturn(ifccParser::InstReturnContext *ctx) {
    LOG_TRACE << "Instruction Return found" << endl;
    return (ASTInstr *) new ASTInstrReturn((ASTExpr *) visit(ctx->expr()));
}

/** Expressions **/
antlrcpp::Any Visitor::visitExprCall(ifccParser::ExprCallContext *ctx) {
    LOG_TRACE << "Function call found";
    ASTExprCall * exprCall = new ASTExprCall(ctx->VAR()->getText());
    for (auto expr : ctx->expr()) {
        exprCall->addAstExpression(visit(expr));
    }
    return (ASTExpr *) exprCall;
}

antlrcpp::Any Visitor::visitExprConst(ifccParser::ExprConstContext *ctx) {
    LOG_TRACE << "Expression Constant found" << endl;
    return (ASTExpr *) new ASTExprConst(new ASTConst(stoi(ctx->CONST()->getText())));
}

antlrcpp::Any Visitor::visitExprVar(ifccParser::ExprVarContext *ctx) {
    LOG_TRACE << "Expression Variable found" << endl;
    return (ASTExpr *) new ASTExprVar(new ASTVar(ctx->VAR()->getText()));
}

antlrcpp::Any Visitor::visitExprAffect(ifccParser::ExprAffectContext *ctx) {
    LOG_TRACE << "Expression Affectation found" << endl;
    return (ASTExpr *) new ASTExprAffect((ASTAffect *) visitAffect(ctx->affect()));
}

antlrcpp::Any Visitor::visitAffect(ifccParser::AffectContext *ctx) {
    LOG_TRACE << "Affectation found" << endl;
    return (ASTAffect *) new ASTAffect(new ASTVar(ctx->VAR()->getText()), (ASTExpr *) visit(ctx->expr()));
}

antlrcpp::Any Visitor::visitDeclVar(ifccParser::DeclVarContext *ctx) {
    LOG_TRACE << "Declaration variable found" << endl;
    return (ASTDecl *) new ASTDeclVar(new ASTVar(ctx->VAR()->getText()));
}

antlrcpp::Any Visitor::visitDeclAffect(ifccParser::DeclAffectContext *ctx) {
    LOG_TRACE << "Declaration affectation found" << endl;
    return (ASTDecl *) new ASTDeclAffect((ASTAffect *) visitAffect(ctx->affect()));
}

antlrcpp::Any Visitor::visitExprMul(ifccParser::ExprMulContext *context) {
    if (context->op->getText() == "*") {
        LOG_TRACE << "Expression multiplication found" << endl;
        return (ASTExpr *) new ASTExprMul((ASTExpr *) visit(context->lExpr), (ASTExpr *) visit(context->rExpr));
    }
    if (context->op->getText() == "/") {
        LOG_TRACE << "Expression division found" << endl;
        return (ASTExpr *) new ASTExprDiv((ASTExpr *) visit(context->lExpr), (ASTExpr *) visit(context->rExpr));
    }
    LOG_TRACE << "Expression modulo found" << endl;
    return (ASTExpr *) new ASTExprMod((ASTExpr *) visit(context->lExpr), (ASTExpr *) visit(context->rExpr));
}

antlrcpp::Any Visitor::visitExprPar(ifccParser::ExprParContext *context) {
    return (ASTExpr*) new ASTExprPar((ASTExpr*) visit(context->expr()));
}

antlrcpp::Any Visitor::visitExprAdd(ifccParser::ExprAddContext *context) {
    if (context->op->getText() == "+") {
        LOG_TRACE << "Expression addition found" << endl;
        return (ASTExpr *) new ASTExprAdd((ASTExpr *) visit(context->lExpr), (ASTExpr *) visit(context->rExpr));
    }
    LOG_TRACE << "Expression minus found" << endl;
    return (ASTExpr *) new ASTExprMin((ASTExpr *) visit(context->lExpr), (ASTExpr *) visit(context->rExpr));
}

antlrcpp::Any Visitor::visitExprCmp(ifccParser::ExprCmpContext* context) {
    if(context->op->getText() == "<") {
        LOG_TRACE << "Expression LT found";
        auto op1 = (ASTExpr*) visit(context->lExpr);
        auto op2 = (ASTExpr*) visit(context->rExpr);
        return (ASTExpr*) new ASTExprLT(op1, op2);
    }
    if(context->op->getText() == "<=") {
        LOG_TRACE << "Expression LE found";
        auto op1 = (ASTExpr*) visit(context->lExpr);
        auto op2 = (ASTExpr*) visit(context->rExpr);
        return (ASTExpr*) new ASTExprLE(op1, op2);
    }
    if(context->op->getText() == ">") {
        LOG_TRACE << "Expression GT found";
        auto op1 = (ASTExpr*) visit(context->lExpr);
        auto op2 = (ASTExpr*) visit(context->rExpr);
        return (ASTExpr*) new ASTExprGT(op1, op2);
    }
    LOG_TRACE << "Expression GE found";
    auto op1 = (ASTExpr*) visit(context->lExpr);
    auto op2 = (ASTExpr*) visit(context->rExpr);
    return (ASTExpr*) new ASTExprGE(op1, op2);
}

antlrcpp::Any Visitor::visitExprEq(ifccParser::ExprEqContext* context) {
    if(context->op->getText() == "==") {
        LOG_TRACE << "Expression Eq found";
        auto op1 = (ASTExpr*) visit(context->lExpr);
        auto op2 = (ASTExpr*) visit(context->rExpr);
        return (ASTExpr*) new ASTExprEq(op1, op2);
    }
    LOG_TRACE << "Expression NE found";
    auto op1 = (ASTExpr*) visit(context->lExpr);
    auto op2 = (ASTExpr*) visit(context->rExpr);
    return (ASTExpr*) new ASTExprNE(op1, op2);
}

antlrcpp::Any Visitor::visitExprAnd(ifccParser::ExprAndContext* context) {
    LOG_TRACE << "Expression And found";
    auto op1 = (ASTExpr*) visit(context->lExpr);
    auto op2 = (ASTExpr*) visit(context->rExpr);
    return (ASTExpr*) new ASTExprAnd(op1, op2);
}

antlrcpp::Any Visitor::visitExprOr(ifccParser::ExprOrContext* context) {
    LOG_TRACE << "Expression Or found";
    auto op1 = (ASTExpr*) visit(context->lExpr);
    auto op2 = (ASTExpr*) visit(context->rExpr);
    return (ASTExpr*) new ASTExprOr(op1, op2);
}

antlrcpp::Any Visitor::visitFunc(ifccParser::FuncContext *ctx) {
    LOG_TRACE << "Function " + ctx->VAR()->getText() + " found";
    auto *astFunc = new ASTFunc(ctx->TYPE()->getText(), ctx->VAR()->getText(), (ASTBloc*) visitBloc(ctx->bloc()));

    for (auto param : ctx->parameter()) {
        astFunc->addParameter(visit(param));
    }

    return astFunc;
}

antlrcpp::Any Visitor::visitParameter(ifccParser::ParameterContext *ctx) {
    LOG_TRACE << "Parameter " + ctx->VAR()->getText();
    return new ASTVar(ctx->VAR()->getText());
}

antlrcpp::Any Visitor::visitInstBloc(ifccParser::InstBlocContext* ctx) {
    LOG_TRACE << "InstBloc found" << endl;
    auto *astBloc = new ASTBloc();
    for (auto instruction : ctx->bloc()->instructions) {
        astBloc->addInstruction(visit(instruction));
    }
    return (ASTInstr *) astBloc;
}

antlrcpp::Any Visitor::visitIf(ifccParser::IfContext* ctx) {
    LOG_TRACE << "if found";
    auto predicate = (ASTExpr*) visit(ctx->predicate);
    auto ifInstr = (ASTInstr*) visit(ctx->ifInst);
    ASTInstr* elseInstr = nullptr;
    if(ctx->elseInst)
        elseInstr = (ASTInstr*) visit(ctx->elseInst);

    auto wrapperBlock = new ASTBloc();
    wrapperBlock->addInstruction((ASTInstr*) new ASTControlIf(predicate, ifInstr, elseInstr));
    return (ASTInstr*) wrapperBlock;
}

antlrcpp::Any Visitor::visitWhile(ifccParser::WhileContext* ctx) {
    LOG_TRACE << "while found";
    auto predicate = (ASTExpr*) visit(ctx->predicate);
    auto instr = (ASTInstr*) visit(ctx->inst());

    auto wrapperBlock = new ASTBloc();
    wrapperBlock->addInstruction((ASTInstr*) new ASTControlWhile(predicate, instr));
    return (ASTInstr*) wrapperBlock;
}

antlrcpp::Any Visitor::visitFor(ifccParser::ForContext* ctx) {
    LOG_TRACE << "for found";

    // process init clause (either decl or expr) and transform into instr
    ASTInstr* initInstr = nullptr;
    if(ctx->initDecl) {
        ASTDecl* decl = (ASTDecl*) visit(ctx->initDecl);
        initInstr = new ASTInstrDecl();
        ((ASTInstrDecl*) initInstr)->addAstDeclaration(decl);
    }
    if(ctx->initExpr) {
        ASTExpr* expr = (ASTExpr*) visit(ctx->initExpr);
        initInstr = new ASTInstrExpr(expr);
    }

    ASTExpr* condition = nullptr;
    if(ctx->condition)
        condition = (ASTExpr*) visit(ctx->condition);
    else
        condition = (ASTExpr*) new ASTExprConst(new ASTConst(1));

    auto instr = (ASTInstr*) visit(ctx->inst());

    auto innerBlock = new ASTBloc();
    innerBlock->addInstruction(instr);

    if(ctx->iteration) {
        auto iteration = new ASTInstrExpr((ASTExpr*)visit(ctx->iteration));
        innerBlock->addInstruction(iteration);
    }

    auto whileInstr = new ASTControlWhile(condition, (ASTInstr*) innerBlock);

    auto wrapperBlock = new ASTBloc();
    if(initInstr)
        wrapperBlock->addInstruction(initInstr);
    wrapperBlock->addInstruction((ASTInstr*) whileInstr);
    return (ASTInstr*) wrapperBlock;
}

antlrcpp::Any Visitor::visitDoWhile(ifccParser::DoWhileContext* ctx) {
    LOG_TRACE << "do while found";
    auto predicate = (ASTExpr*)visit(ctx->predicate);
    auto instr = (ASTInstr*)visit(ctx->inst());

    auto wrapperBlock = new ASTBloc();
    wrapperBlock->addInstruction((ASTInstr*) new ASTControlDoWhile(predicate, instr));
    return (ASTInstr*) wrapperBlock;
}

antlrcpp::Any Visitor::visitExprNeg(ifccParser::ExprNegContext* ctx) {
    return (ASTExpr*) new ASTExprNeg((ASTExpr*) visit(ctx->expr()));
}
