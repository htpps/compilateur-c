
#include <IR.h>

#include "IR.h"
#include "Logging.h"
#include "ast/AST.h"

using namespace std;

/**
 * CFG methods
 */
CFG::CFG(ASTProg *ast, SymbolTable *symbolTable) : ast(ast), symbolTable_(symbolTable), nextFreeSymbolIndex(0) {}

void CFG::gen_asm_prologue(ostream& o){
    //context_stack.push_back({"main"});
    //getSymbolTable()->setContext({{"main"}});
    o << "\t.globl main\n";
    o << "main:\n";
    o << "# Prologue\n";
    o << "\tpushq\t%rbp\n";
    o << "\tmovq\t%rsp,\t%rbp\n";
    int mem = getSymbolTable()->getMemorySpaceInContext("/main");
    int space_allocated = 16*(mem/16) + 16;
    o << "\tsubq\t$" << space_allocated << ",\t%rsp\n";
}

void CFG::gen_asm_epilogue(ostream& o){
    o << "# Epilogue\n";
    o << "\tleave\n";
    o << "\tret\n";
    /*context_stack.pop_back();
    if(context_stack.size()>0){
        getSymbolTable()->setContext(context_stack.back());
    }*/
}

void CFG::gen_asm(ostream& o){
    o << "\t.text\n";

    // Build IR
    ast->buildIR(this);
    // Generate asm
    for(BasicBlock* basicBlock:bbs){

        if(basicBlock->label == "main"){
            gen_asm_prologue(o);
        }

        if(basicBlock->label != "main" && basicBlock->label.substr(0,2) == ".L") // TODO modifier la génération du prologue
            o << basicBlock->label << ":\n";

        basicBlock->gen_asm(o);
        if(basicBlock->exit_true != nullptr) {
            if(basicBlock->exit_false == nullptr)
                o << "\tjmp\t" << basicBlock->exit_true->label << '\n';
            else {
                if(basicBlock->test_var_name.empty())
                    throw GenericException(ExceptionType::BasicBlockMissingTestVar, "Basic block missing test var");
                o << "\tcmpl\t$0,\t" << basicBlock->test_var_name << '\n'
                  << "\tjne\t" << basicBlock->exit_true->label << '\n'
                  << "\tjmp\t" << basicBlock->exit_false->label << '\n';
            }
        }
    }

    // Generate ASMR epilogue
    gen_asm_epilogue(o);
}

void CFG::addBB(BasicBlock* bb){
    bbs.push_back(bb);
}

BasicBlock* CFG::getCurrentBB(){
    return current_bb;
}

void CFG::setCurrentBB(BasicBlock* bb){
    current_bb= bb;
}

std::string CFG::create_new_tempvar(SymbolType t){
    string tmpName("tmp_" + to_string(nextFreeSymbolIndex++));
    symbolTable_->addSymbol(new Symbol(tmpName, SymbolType::INT));
    return tmpName;
}

string CFG::IR_reg_to_asm(string reg) {
    if(reg == "eax"){
        return "%eax";
    }
    return "-" + to_string(symbolTable_->getSymbol(reg)->getAddress()) + "(%rbp)"; //TODO check that false is good!
}

std::string CFG::nextBlockLabel() const {
    static int blockCpt = 0;
    return ".L" + to_string(blockCpt++);
}

BasicBlock* CFG::newBB() {
    auto* bb = new BasicBlock(this, this->nextBlockLabel());
    bbs.push_back(bb);
    return bb;
}

SymbolTable *CFG::getSymbolTable() {
    return symbolTable_;
}

void CFG::dumpIR() {
    LOG_TRACE << "##########DUMP IR##########";
    for(auto bb:bbs){
        LOG_TRACE << "------------BB " << bb->label << "---------------";
        for(IRInstr* instr:bb->instrs){
            cout << *instr << endl;
        }
    }
}

/**
 * BasicBlock methods
 */
BasicBlock::BasicBlock(CFG* cfg, string entry_label)
: cfg(cfg), label(entry_label), exit_true(nullptr), exit_false(nullptr) {}

void BasicBlock::add_IRInstr(IRInstr::Operation op, SymbolType t, vector<string> params){
    instrs.push_back(new IRInstr(this, op, t, params));
}

void BasicBlock::gen_asm(ostream &o) {
    for(IRInstr* instr: instrs){
        instr->gen_asm(o);
    }
}
/**
 * IRInstr methods
 */
IRInstr::IRInstr(BasicBlock* bb, Operation op, SymbolType t, vector<string> params)
: bb(bb), op(op),type(t),params(std::move(params)) {
    regx86 = {"edi", "esi", "edx", "ecx", "r8d", "r9d"};
}

void IRInstr::gen_asm(std::ostream &o) {
    switch(op){
        // bb->cfg : On remonte au cfg via l'intermédiaire du bb (et c'est dégueu) pour accéder à la fonction qui converti
        // ...le nom de variable en addresse en mémoire
        case IRInstr::Operation::ldconst:
            // TODO : Faire plus propre
            // params[0] = la destination, params[1] = la constante
            o << "\tmovl\t$" + params[1] + ",\t" + bb->cfg->IR_reg_to_asm(params[0]) + "\n";
            break;
        case IRInstr::Operation::copy:
            // TODO : Faire plus propre
            // params[0] = la destination, params[1] = la case mémoire
            o << "\tmovl\t" +bb->cfg->IR_reg_to_asm(params[1]) + ",\t" + bb->cfg->IR_reg_to_asm(params[0]) + "\n";
            break;
        case IRInstr::Operation::add:
            //params[0] = destination, params[1] = op 1, params[2] = op 2
            o << "\tmovl\t" + bb->cfg->IR_reg_to_asm(params[1]) + ",\t%edx\n";
            o << "\tmovl\t" + bb->cfg->IR_reg_to_asm(params[2]) + ",\t%eax\n";
            o << "\taddl\t%edx,\t%eax\n";
            o << "\tmovl\t%eax,\t" + bb->cfg->IR_reg_to_asm(params[0]) + "\n";
            break;
        case IRInstr::Operation::mul:
            //params[0] = destination, params[1] = op 1, params[2] = op 2
            o << "\tmovl\t" + bb->cfg->IR_reg_to_asm(params[1]) + ",\t%edx\n";
            o << "\tmovl\t" + bb->cfg->IR_reg_to_asm(params[2]) + ",\t%eax\n";
            o << "\timull\t%edx,\t%eax\n";
            o << "\tmovl\t%eax,\t" + bb->cfg->IR_reg_to_asm(params[0]) + "\n";
            break;
        case IRInstr::Operation::sub:
            //params[0] = destination, params[1] = op 1, params[2] = op 2
            o << "\tmovl\t" + bb->cfg->IR_reg_to_asm(params[1]) + ",\t%eax\n";
            o << "\tmovl\t" + bb->cfg->IR_reg_to_asm(params[2]) + ",\t%edx\n";
            o << "\tsubl\t%edx,\t%eax\n";
            o << "\tmovl\t%eax,\t" + bb->cfg->IR_reg_to_asm(params[0]) + "\n";
            break;
        case IRInstr::Operation::div:
            //params[0] = destination, params[1] = op 1, params[2] = op 2
            o << "\tmovl\t" + bb->cfg->IR_reg_to_asm(params[1]) + ",\t%eax\n";
            o << "\tmovl\t" + bb->cfg->IR_reg_to_asm(params[2]) + ",\t%ecx\n";
            o << "\tcltd\n";
            o << "\tidivl\t%ecx\n";
            o << "\tmovl\t%eax,\t" + bb->cfg->IR_reg_to_asm(params[0]) + "\n";
            break;
        case IRInstr::Operation::mod:
            //params[0] = destination, params[1] = op 1, params[2] = op 2
            o << "\tmovl\t" + bb->cfg->IR_reg_to_asm(params[1]) + ",\t%eax\n";
            o << "\tmovl\t" + bb->cfg->IR_reg_to_asm(params[2]) + ",\t%ecx\n";
            o << "\tcltd\n";
            o << "\tidivl\t%ecx\n";
            o << "\tmovl\t%edx,\t" + bb->cfg->IR_reg_to_asm(params[0]) +"\n";
            break;
        case IRInstr::Operation::neg: {
            auto res = bb->cfg->IR_reg_to_asm(params[0]);
            auto op1 = bb->cfg->IR_reg_to_asm(params[1]);
            o << "\tmovl\t" << op1 << ",\t%eax\n"
              << "\tnegl\t%eax\n"
              << "\tmovl\t%eax,\t" << res << '\n';
            break;
        }
        case IRInstr::Operation::cmp_le: {
            auto res = bb->cfg->IR_reg_to_asm(params[0]);
            auto op1 = bb->cfg->IR_reg_to_asm(params[1]);
            auto op2 = bb->cfg->IR_reg_to_asm(params[2]);
            o << "\tmovl\t" << op1 << ",\t%eax\n"
              << "\tcmpl\t" << op2 << ",\t%eax\n"
              << "\tsetle\t%al\n"
              << "\tmovzbl\t%al,\t%eax\n"
              << "\tmovl\t%eax,\t" << res << '\n';
            break;
        }
        case IRInstr::Operation::cmp_lt: {
            auto res = bb->cfg->IR_reg_to_asm(params[0]);
            auto op1 = bb->cfg->IR_reg_to_asm(params[1]);
            auto op2 = bb->cfg->IR_reg_to_asm(params[2]);
            o << "\tmovl\t" << op1 << ",\t%eax\n"
              << "\tcmpl\t" << op2 << ",\t%eax\n"
              << "\tsetl\t%al\n"
              << "\tmovzbl\t%al,\t%eax\n"
              << "\tmovl\t%eax,\t" << res << '\n'
              << "\tmovl\t$0,\t%eax\n";
            break;
        }
        case IRInstr::Operation::cmp_ge: {
            auto res = bb->cfg->IR_reg_to_asm(params[0]);
            auto op1 = bb->cfg->IR_reg_to_asm(params[1]);
            auto op2 = bb->cfg->IR_reg_to_asm(params[2]);
            o << "\tmovl\t" << op1 << ",\t%eax\n"
              << "\tcmpl\t" << op2 << ",\t%eax\n"
              << "\tsetge\t%al\n"
              << "\tmovzbl\t%al,\t%eax\n"
              << "\tmovl\t%eax,\t" << res << '\n'
              << "\tmovl\t$0,\t%eax\n";
            break;
        }
        case IRInstr::Operation::cmp_gt: {
            auto res = bb->cfg->IR_reg_to_asm(params[0]);
            auto op1 = bb->cfg->IR_reg_to_asm(params[1]);
            auto op2 = bb->cfg->IR_reg_to_asm(params[2]);
            o << "\tmovl\t" << op1 << ",\t%eax\n"
              << "\tcmpl\t" << op2 << ",\t%eax\n"
              << "\tsetg\t%al\n"
              << "\tmovzbl\t%al,\t%eax\n"
              << "\tmovl\t%eax,\t" << res << '\n'
              << "\tmovl\t$0,\t%eax\n";
            break;
        }
        case IRInstr::Operation::cmp_eq: {
            auto res = bb->cfg->IR_reg_to_asm(params[0]);
            auto op1 = bb->cfg->IR_reg_to_asm(params[1]);
            auto op2 = bb->cfg->IR_reg_to_asm(params[2]);
            o << "\tmovl\t" << op1 << ",\t%eax\n"
              << "\tcmpl\t" << op2 << ",\t%eax\n"
              << "\tsete\t%al\n"
              << "\tmovzbl\t%al,\t%eax\n"
              << "\tmovl\t%eax,\t" << res << '\n'
              << "\tmovl\t$0,\t%eax\n";
            break;
        }
        case IRInstr::Operation::cmp_ne: {
            auto res = bb->cfg->IR_reg_to_asm(params[0]);
            auto op1 = bb->cfg->IR_reg_to_asm(params[1]);
            auto op2 = bb->cfg->IR_reg_to_asm(params[2]);
            o << "\tmovl\t" << op1 << ",\t%eax\n"
              << "\tcmpl\t" << op2 << ",\t%eax\n"
              << "\tsetne\t%al\n"
              << "\tmovzbl\t%al,\t%eax\n"
              << "\tmovl\t%eax,\t" << res << '\n'
              << "\tmovl\t$0,\t%eax\n";
            break;
        }
        case IRInstr::Operation::call: {
            /*
                pushq	%rbp
                movq	%rsp, %rbp
                subq	$32, %rsp    // taille pour la fonction
             */


            int nb_register = 0;
            for(auto it=params.begin()+2;it != params.end();it++){
                o << "\tmovl\t" + bb->cfg->IR_reg_to_asm(*it) + ",\t %" + regx86[nb_register++] + "\n";
            }
            o << "\tcall\t" << params[1] << "\n";
            o << "\tmovl\t %eax,\t" +  bb->cfg->IR_reg_to_asm(params[0]) + "\n";
            //l
            break;
        }
        case IRInstr::Operation::function: {

            // Setting symbol table context
            bb->cfg->context_stack.push_back({params[0]});
            bb->cfg->getSymbolTable()->setContext({params[0]});

            int nb_register = 0;

            o << "\t.globl\t"	<< params [0] + "\n";
            o << params [0] << ":\n";

            // prolog
            o << "\tpushq\t%rbp\n";
            o << "\tmovq\t%rsp,\t%rbp\n";

            // params
            for(auto it = params.begin() + 1; it != params.end(); it++){
                o << "\tmovl\t %" + regx86[nb_register++] + ",\t" + bb->cfg->IR_reg_to_asm(*it) + "\n";
            }
            break;
        }
        case IRInstr::Operation::ret:{
            // Reset context
            bb->cfg->context_stack.pop_back();
            if(bb->cfg->context_stack.size()>0){
                bb->cfg->getSymbolTable()->setContext(bb->cfg->context_stack.back());
            }
            bb->cfg->getSymbolTable()->closeContext();

            o << "# Epilogue\n";
            o << "\tpopq\t%rbp\n";
            o << "\tret\n";
            break;
        }
        case IRInstr::Operation::open_context: {
            LOG_TRACE << "Opening context " + params[0] + " in gen_asm";
            bb->cfg->getSymbolTable()->openContext(params[0]);
            break;
        }
        case IRInstr::Operation::close_context: {
            bb->cfg->getSymbolTable()->closeContext();
            break;
        }
        default:
            LOG_FATAL << "ASM not implemented for operation " << op;
            break;
    }

}

ostream &operator<<(ostream &os, const IRInstr &instr) {

    os << "bb: " << instr.bb->label << " op: " << instr.op << " type: " << instr.type << " params: " ;
    for(string param:instr.params){
        os << param << ",";
    }
    return os;
}

