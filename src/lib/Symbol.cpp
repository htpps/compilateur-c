//
// Created by mathieu on 25/03/2020.
//

#include <Symbol.h>
#include <Exceptions.h>

#include "Symbol.h"

using namespace std;

Symbol::Symbol(std::string name, SymbolType type) : name(std::move(name)), type(type){}
Symbol::Symbol(std::string name, SymbolType type, int address) : name(std::move(name)), type(type), address(address){}

bool Symbol::operator==(const Symbol &rhs) const {
    return name == rhs.name &&
           type == rhs.type &&
           address == rhs.address;
}

bool Symbol::operator!=(const Symbol &rhs) const {
    return !(rhs == *this);
}

const std::string& Symbol::getName() const {
    return name;
}

int Symbol::getAddress() const {
    return address;
}

void Symbol::setAddress(int address) {
    Symbol::address = address;
}

SymbolType Symbol::getType() const {
    return type;
}

std::ostream &operator<<(std::ostream &os, const Symbol &symbol) {
    os << "name: " << symbol.name << " type: " << symbol.type << " address: " << symbol.address;
    return os;
}

std::string Symbol::SymbolTypeToString(SymbolType symbolType) {
    switch(symbolType){
        case SymbolType::INT : return "INT";
        default : return "[Unknown symbol_type]";
    }
}

SymbolType Symbol::SymbolStringToType(const std::string& symbolString) {
    if(symbolString == "int"){
        return SymbolType::INT;
    }
    else{
        throw GenericException(ExceptionTypeEnum::UnsupportedTypeException, "Tried to get a type that is invalid : " + symbolString);
    }
}

bool Symbol::isUsed() const {
    return used;
}

void Symbol::setUsed(bool used) {
    Symbol::used = used;
}

bool Symbol::isInitialized() const {
    return initialized;
}

void Symbol::setInitialized(bool initialized) {
    Symbol::initialized = initialized;
}
