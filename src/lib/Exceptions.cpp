#include "Exceptions.h"

#include <utility>

GenericException::GenericException(ExceptionType exceptionType, std::string message) : exceptionType(exceptionType), message(std::move(message)) {}

const ExceptionType& GenericException::getExceptionType() const {
    return exceptionType;
}

const char* GenericException::what() const noexcept {
    return message.c_str();
}