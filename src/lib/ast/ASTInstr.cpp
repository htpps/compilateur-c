//
// Created by matmont98 on 01/04/2020.
//


// *** ASTInstrReturn ***

#include "ast/AST.h"
#include "IR.h"

using namespace std;

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTInstrExpr                                                          //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTInstrExpr::ASTInstrExpr(ASTExpr *expression) : expression(expression) {}

string ASTInstrExpr::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"InstExpr\"]" + ";\n";
    expr += to_string(nodeID) + " -- " + to_string(nodeID + 1) + ";\n";
    expr += expression->getDotRepresentation();
    return expr;
}

void ASTInstrExpr::process(VariableChecker &variableChecker) {
    expression->process(variableChecker);
}

std::string ASTInstrExpr::buildIR(CFG *cfg) {
    return expression->buildIR(cfg);
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTInstrDecl                                                          //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

string ASTInstrDecl::getDotRepresentation() {

    string res(to_string(++nodeID) + " [label=\"InstDecl\"]" + ";\n");
    int idParent = nodeID;
    for (auto &decl : declarations) {
        res += to_string(idParent) + " -- " + to_string(nodeID + 1) + ";\n";
        res += decl->getDotRepresentation();
    }
    return res;
}

void ASTInstrDecl::addAstDeclaration(ASTDecl *astDecl) {
    declarations.push_back(astDecl);
}

void ASTInstrDecl::process(VariableChecker &variableChecker) {
    for (ASTDecl *decl: declarations) {
        decl->process(variableChecker);
    }
}

std::string ASTInstrDecl::buildIR(CFG *cfg) {
    for (auto &decl: declarations) {
        decl->buildIR(cfg);
    }
    return "";
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTInstrReturn                                                        //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTInstrReturn::ASTInstrReturn(ASTExpr* expression) : expression(expression) {}

string ASTInstrReturn::getDotRepresentation() {
    string res(to_string(++nodeID) + " [label=\"INSTR RETURN\"]" + ";\n");
    res += to_string(nodeID) + " -- " + to_string(nodeID + 1) + ";\n";
    res += expression->getDotRepresentation();
    return res;
}

void ASTInstrReturn::process(VariableChecker &variableChecker) { // Return statement, variables contained in expression are used.
    variableChecker.setOnTheRight(true);
    expression->process(variableChecker);
    variableChecker.setOnTheRight(false);
}

// Construit l'instruction
string ASTInstrReturn::buildIR(CFG *cfg) {
    string label(expression->buildIR(cfg)); //tmpvar
    BasicBlock *currentBB = cfg->getCurrentBB();
    vector<string> params;

    params.emplace_back("eax");
    params.push_back(label);
    currentBB->add_IRInstr(IRInstr::Operation::copy, SymbolType::INT, params);
    return "";
}