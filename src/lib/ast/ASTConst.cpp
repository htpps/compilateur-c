//
// Created by matmont98 on 01/04/2020.
//

#include "ast/AST.h"
#include "IR.h"

using namespace std;

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTConst                                                              //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTConst::ASTConst(int value) : value(value) {}

string ASTConst::getDotRepresentation() {
    return to_string(++nodeID) + " [label=\"" + to_string(value) + "\"]" + ";\n";
}

int ASTConst::getValue() const {
    return value;
}

void ASTConst::process(VariableChecker& variableChecker) {}

string ASTConst::buildIR(CFG* cfg){
    return "";
}
