//
// Created by soplump on 06/04/2020.
//

#include "ast/ASTExprCall.h"

#include <utility>
#include "IR.h"

using namespace std;

ASTExprCall::ASTExprCall(string label) : label(std::move(label)) {}


void ASTExprCall::process(VariableChecker& variableChecker) {
    // Check que les paramêtres soient initialisées.

    variableChecker.doesFunctionExist(label, expressions.size());

    for(auto &it : expressions){
        it->process(variableChecker);
    }
}

std::string ASTExprCall::getDotRepresentation() {
    string res(to_string(++nodeID) + " [label=\"ExprCall: " + label +"\"]" + ";\n");
    int idParent = nodeID;
    for (auto &decl : expressions) {
        res += to_string(idParent) + " -- " + to_string(nodeID + 1) + ";\n";
        res += decl->getDotRepresentation();
    }
    return res;
}

std::string ASTExprCall::buildIR(CFG* cfg) {
    vector<string> params;
    string callVar = cfg->create_new_tempvar(SymbolType::INT);
    params = { callVar, label };

    for (ASTExpr* expression : expressions) {
        string tmpvar = expression->buildIR(cfg);
        params.push_back(tmpvar);
    }

    cfg->getCurrentBB()->add_IRInstr(IRInstr::Operation::call, SymbolType::INT, params);
    return callVar;
}

void ASTExprCall::addAstExpression(ASTExpr * expr) {
    expressions.push_back(expr);
}
