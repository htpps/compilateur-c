//
// Created by linux on 01/04/2020.
//

#include "ast/ASTTreeRepr.h"
#include <sstream>

int nodeID = 0;
using namespace std;

string getTreeRepresentation(ASTNode* rootNode) {
    stringstream treeRepresentation;
    treeRepresentation << "graph AST { ";

    string nodeRepresentations(rootNode->getDotRepresentation());
    treeRepresentation << nodeRepresentations;

    treeRepresentation << "\n}";
    return treeRepresentation.str();
}
