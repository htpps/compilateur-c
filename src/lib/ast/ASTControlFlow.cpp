#include "ast/AST.h"
#include "Logging.h"
#include "IR.h"
#include <string>
#include <ast/ASTControlFlow.h>


using namespace std;

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTControlIf                                                          //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTControlIf::ASTControlIf(ASTExpr* predicate, ASTInstr* ifInstr, ASTInstr* elseInstr) : predicate(predicate),
                                                                                         ifInstr(ifInstr),
                                                                                         elseInstr(elseInstr) {}

std::string ASTControlIf::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"if\"];\n";
    int id = nodeID;
    expr += to_string(id) + " -- " + to_string(nodeID + 1) + " [label=\"predicate\",labelfloat=\"true\"];\n";
    expr += predicate->getDotRepresentation();
    expr += to_string(id) + " -- " + to_string(nodeID + 1) + " [label=\"if block\",labelfloat=\"true\"];\n";
    expr += ifInstr->getDotRepresentation();
    if(elseInstr) {
        expr += to_string(id) + " -- " + to_string(nodeID + 1) + " [label=\"else block\",labelfloat=\"true\"];\n";
        expr += elseInstr->getDotRepresentation();
    }
    return expr;
}

void ASTControlIf::process(VariableChecker& checker) {
    predicate->process(checker);
    ifInstr->process(checker);
    if(elseInstr)
        elseInstr->process(checker);
}

std::string ASTControlIf::buildIR(CFG* cfg) {

    auto ifBlock = new BasicBlock(cfg, cfg->nextBlockLabel());
    auto elseBlock = new BasicBlock(cfg, cfg->nextBlockLabel());
    auto afterIf = new BasicBlock(cfg, cfg->nextBlockLabel());

    auto predValue = predicate->buildIR(cfg);
    auto currentBB = cfg->getCurrentBB();
    currentBB->test_var_name = cfg->IR_reg_to_asm(predValue);
    currentBB->exit_true = ifBlock;
    currentBB->exit_false = elseInstr ? elseBlock : afterIf;
    ifBlock->exit_true = afterIf;
    elseBlock->exit_true = afterIf;

    cfg->addBB(ifBlock);
    cfg->setCurrentBB(ifBlock);
    ifInstr->buildIR(cfg);

    if(elseInstr) {
        cfg->addBB(elseBlock);
        cfg->setCurrentBB(elseBlock);
        elseInstr->buildIR(cfg);
    }

    cfg->addBB(afterIf);
    cfg->setCurrentBB(afterIf);

    return "";
}

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTControlWhile                                                       //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTControlWhile::ASTControlWhile(ASTExpr* predicate, ASTInstr* instr) : predicate(predicate), instr(instr) {}

std::string ASTControlWhile::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"while\"];\n";
    int id = nodeID;
    expr += to_string(id) + " -- " + to_string(nodeID + 1) + " [label=\"predicate\",labelfloat=\"true\"];\n";
    expr += predicate->getDotRepresentation();
    expr += to_string(id) + " -- " + to_string(nodeID + 1) + ";\n";
    expr += instr->getDotRepresentation();
    return expr;
}

void ASTControlWhile::process(VariableChecker& checker) {
    predicate->process(checker);
    instr->process(checker);
}

std::string ASTControlWhile::buildIR(CFG* cfg) {
    auto beforeWhile = cfg->newBB();
    auto whileBlock = cfg->newBB();
    auto afterWhile = cfg->newBB();

    cfg->getCurrentBB()->exit_true = beforeWhile;
    cfg->setCurrentBB(beforeWhile);
    auto predValue = predicate->buildIR(cfg);
    beforeWhile->test_var_name = cfg->IR_reg_to_asm(predValue);
    beforeWhile->exit_true = whileBlock;
    beforeWhile->exit_false = afterWhile;
    whileBlock->exit_true = beforeWhile;

    cfg->setCurrentBB(whileBlock);
    instr->buildIR(cfg);

    cfg->setCurrentBB(afterWhile);

    return "";
}


//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTControlDoWhile                                                     //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTControlDoWhile::ASTControlDoWhile(ASTExpr* predicate, ASTInstr* instr) : predicate(predicate), instr(instr) {}

std::string ASTControlDoWhile::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"do while\"];\n";
    int id = nodeID;
    expr += to_string(id) + " -- " + to_string(nodeID + 1) + " [label=\"predicate\",labelfloat=\"true\"];\n";
    expr += predicate->getDotRepresentation();
    expr += to_string(id) + " -- " + to_string(nodeID + 1) + ";\n";
    expr += instr->getDotRepresentation();
    return expr;
}

void ASTControlDoWhile::process(VariableChecker& checker) {
    predicate->process(checker);
    instr->process(checker);
}

std::string ASTControlDoWhile::buildIR(CFG* cfg) {
    auto whileBlock = cfg->newBB();
    auto afterWhile = cfg->newBB();

    cfg->getCurrentBB()->exit_true = whileBlock;
    cfg->setCurrentBB(whileBlock);
    instr->buildIR(cfg);
    auto predValue = predicate->buildIR(cfg);
    whileBlock->test_var_name = cfg->IR_reg_to_asm(predValue);
    whileBlock->exit_true = whileBlock;
    whileBlock->exit_false = afterWhile;

    cfg->setCurrentBB(afterWhile);

    return "";
}
