//
// Created by matmont98 on 01/04/2020.
//

#include "Logging.h"
#include "ast/AST.h"
#include "IR.h"

using namespace std;

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTVar                                                                //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTVar::ASTVar(string name) : name(move(name)) , onTheRight(false){}

// TODO à changer (pb avec l'énum)
string ASTVar::getDotRepresentation() {
    return to_string(++nodeID) + " [label=\"" + "int " + name + "\"]" + ";\n";
}

const string &ASTVar::getName() const {
    return name;
}

void ASTVar::process(VariableChecker &variableChecker) {
    LOG_TRACE << "Var " + name + " set to be on the right";
    variableChecker.setSymbolInTableUsed(name);
}

string ASTVar::buildIR(CFG* cfg){
    return "";
}