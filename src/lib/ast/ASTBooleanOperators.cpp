#include "ast/AST.h"
#include "Logging.h"
#include "IR.h"
#include <string>

using namespace std;

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprLE                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprLE::ASTExprLE(ASTExpr* leftExpression, ASTExpr* rightExpression) : ASTExprBin(leftExpression, rightExpression) {}

std::string ASTExprLE::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"<=\"];\n";
    return expr + ASTExprBin::getDotRepresentation();
}

std::string ASTExprLE::buildIR(CFG* cfg) {
    string op1(leftExpression->buildIR(cfg));
    string op2(rightExpression->buildIR(cfg));
    BasicBlock *currentBB = cfg->getCurrentBB();
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op1);
    params.push_back(op2);

    currentBB->add_IRInstr(IRInstr::Operation::cmp_le, SymbolType::INT, params);
    return resName;
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprLT                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprLT::ASTExprLT(ASTExpr* leftExpression, ASTExpr* rightExpression) : ASTExprBin(leftExpression, rightExpression) {}

std::string ASTExprLT::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"<\"];\n";
    return expr + ASTExprBin::getDotRepresentation();
}

std::string ASTExprLT::buildIR(CFG* cfg) {
    string op1(leftExpression->buildIR(cfg));
    string op2(rightExpression->buildIR(cfg));
    BasicBlock *currentBB = cfg->getCurrentBB();
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op1);
    params.push_back(op2);

    currentBB->add_IRInstr(IRInstr::Operation::cmp_lt, SymbolType::INT, params);
    return resName;
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprGE                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprGE::ASTExprGE(ASTExpr* leftExpression, ASTExpr* rightExpression) : ASTExprBin(leftExpression, rightExpression) {}

std::string ASTExprGE::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\">=\"];\n";
    return expr + ASTExprBin::getDotRepresentation();
}

std::string ASTExprGE::buildIR(CFG* cfg) {
    string op1(leftExpression->buildIR(cfg));
    string op2(rightExpression->buildIR(cfg));
    BasicBlock *currentBB = cfg->getCurrentBB();
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op1);
    params.push_back(op2);

    currentBB->add_IRInstr(IRInstr::Operation::cmp_ge, SymbolType::INT, params);
    return resName;
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprGT                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprGT::ASTExprGT(ASTExpr* leftExpression, ASTExpr* rightExpression) : ASTExprBin(leftExpression, rightExpression) {}

std::string ASTExprGT::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\">\"];\n";
    return expr + ASTExprBin::getDotRepresentation();
}

std::string ASTExprGT::buildIR(CFG* cfg) {
    string op1(leftExpression->buildIR(cfg));
    string op2(rightExpression->buildIR(cfg));
    BasicBlock *currentBB = cfg->getCurrentBB();
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op1);
    params.push_back(op2);

    currentBB->add_IRInstr(IRInstr::Operation::cmp_gt, SymbolType::INT, params);
    return resName;
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprEq                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprEq::ASTExprEq(ASTExpr* leftExpression, ASTExpr* rightExpression) : ASTExprBin(leftExpression, rightExpression) {}

std::string ASTExprEq::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"==\"];\n";
    return expr + ASTExprBin::getDotRepresentation();
}

std::string ASTExprEq::buildIR(CFG* cfg) {
    string op1(leftExpression->buildIR(cfg));
    string op2(rightExpression->buildIR(cfg));
    BasicBlock *currentBB = cfg->getCurrentBB();
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op1);
    params.push_back(op2);

    currentBB->add_IRInstr(IRInstr::Operation::cmp_eq, SymbolType::INT, params);
    return resName;
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprNE                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprNE::ASTExprNE(ASTExpr* leftExpression, ASTExpr* rightExpression) : ASTExprBin(leftExpression, rightExpression) {}

std::string ASTExprNE::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"!=\"];\n";
    return expr + ASTExprBin::getDotRepresentation();
}

std::string ASTExprNE::buildIR(CFG* cfg) {
    string op1(leftExpression->buildIR(cfg));
    string op2(rightExpression->buildIR(cfg));
    BasicBlock *currentBB = cfg->getCurrentBB();
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op1);
    params.push_back(op2);

    currentBB->add_IRInstr(IRInstr::Operation::cmp_ne, SymbolType::INT, params);
    return resName;
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprAnd                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprAnd::ASTExprAnd(ASTExpr* leftExpression, ASTExpr* rightExpression) : ASTExprBin(leftExpression, rightExpression) {}

std::string ASTExprAnd::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"&&\"];\n";
    return expr + ASTExprBin::getDotRepresentation();
}

std::string ASTExprAnd::buildIR(CFG* cfg) {
    string resName = cfg->create_new_tempvar(SymbolType::INT);
    vector<string> params;

    auto firstOpTrue = cfg->newBB();
    auto andFalse = cfg->newBB();
    auto andTrue = cfg->newBB();
    auto afterAnd = cfg->newBB();

    params.push_back(resName);
    params.emplace_back("0");
    andFalse->add_IRInstr(IRInstr::Operation::ldconst, SymbolType::INT, params);
    andFalse->exit_true = afterAnd;

    params.clear();
    params.push_back(resName);
    params.emplace_back("1");
    andTrue->add_IRInstr(IRInstr::Operation::ldconst, SymbolType::INT, params);
    andTrue->exit_true = afterAnd;

    // test 1st operand
    string op1(leftExpression->buildIR(cfg));
    cfg->getCurrentBB()->test_var_name = cfg->IR_reg_to_asm(op1);

    cfg->getCurrentBB()->exit_false = andFalse;
    cfg->getCurrentBB()->exit_true = firstOpTrue;

    // test 2nd operand
    string op2(rightExpression->buildIR(cfg));
    firstOpTrue->test_var_name = cfg->IR_reg_to_asm(op2);

    firstOpTrue->exit_false = andFalse;
    firstOpTrue->exit_true = andTrue;

    cfg->setCurrentBB(afterAnd);

    return resName;
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprNE                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//
ASTExprOr::ASTExprOr(ASTExpr* leftExpression, ASTExpr* rightExpression) : ASTExprBin(leftExpression, rightExpression) {}

std::string ASTExprOr::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"||\"];\n";
    return expr + ASTExprBin::getDotRepresentation();
}

std::string ASTExprOr::buildIR(CFG* cfg) {
    string resName = cfg->create_new_tempvar(SymbolType::INT);
    vector<string> params;

    auto firstOpFalse = cfg->newBB();
    auto orFalse = cfg->newBB();
    auto orTrue = cfg->newBB();
    auto afterOr = cfg->newBB();

    params.push_back(resName);
    params.emplace_back("0");
    orFalse->add_IRInstr(IRInstr::Operation::ldconst, SymbolType::INT, params);
    orFalse->exit_true = afterOr;

    params.clear();
    params.push_back(resName);
    params.emplace_back("1");
    orTrue->add_IRInstr(IRInstr::Operation::ldconst, SymbolType::INT, params);
    orTrue->exit_true = afterOr;

    // test 1st operand
    string op1(leftExpression->buildIR(cfg));
    cfg->getCurrentBB()->test_var_name = cfg->IR_reg_to_asm(op1);

    cfg->getCurrentBB()->exit_false = firstOpFalse;
    cfg->getCurrentBB()->exit_true = orTrue;

    // test 2nd operand
    string op2(rightExpression->buildIR(cfg));
    firstOpFalse->test_var_name = cfg->IR_reg_to_asm(op2);

    firstOpFalse->exit_false = orFalse;
    firstOpFalse->exit_true = orTrue;

    cfg->setCurrentBB(afterOr);

    return resName;
}
