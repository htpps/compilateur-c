//
// Created by matmont98 on 01/04/2020.
//

#include "ast/AST.h"
#include "IR.h"

using namespace std;

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprBin                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprBin::ASTExprBin(ASTExpr *leftExpression, ASTExpr *rightExpression) : leftExpression(leftExpression),
                                                                            rightExpression(rightExpression) {}
string ASTExprBin::getDotRepresentation() {
    int id = nodeID;
    string expr = to_string(id) + " -- " + to_string(nodeID + 1) + ";\n";
    expr += leftExpression->getDotRepresentation();
    expr += to_string(id) + " -- " + to_string(nodeID + 1) + ";\n";
    expr += rightExpression->getDotRepresentation();
    return expr;
}

void ASTExprBin::process(VariableChecker& variableChecker){
    leftExpression->process(variableChecker);
    rightExpression->process(variableChecker);
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprMul                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprMul::ASTExprMul(ASTExpr *leftExpression, ASTExpr *rightExpression) : ASTExprBin(leftExpression,rightExpression) {}

string ASTExprMul::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"*\"]" + ";\n";
    return expr + ASTExprBin::getDotRepresentation();
}

string ASTExprMul::buildIR(CFG* cfg){
    string op1(leftExpression->buildIR(cfg));
    string op2(rightExpression->buildIR(cfg));
    BasicBlock *currentBB = cfg->getCurrentBB();
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op1);
    params.push_back(op2);

    currentBB->add_IRInstr(IRInstr::Operation::mul, SymbolType::INT, params);
    return resName;
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprDiv                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprDiv::ASTExprDiv(ASTExpr *leftExpression, ASTExpr *rightExpression) : ASTExprBin(leftExpression,rightExpression){}

string ASTExprDiv::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"/\"]" + ";\n";
    return expr + ASTExprBin::getDotRepresentation();
}

string ASTExprDiv::buildIR(CFG* cfg){
    string op1(leftExpression->buildIR(cfg));
    string op2(rightExpression->buildIR(cfg));
    BasicBlock *currentBB = cfg->getCurrentBB();
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op1);
    params.push_back(op2);

    currentBB->add_IRInstr(IRInstr::Operation::div, SymbolType::INT, params);
    return resName;
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprMin                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprMin::ASTExprMin(ASTExpr *leftExpression, ASTExpr *rightExpression) : ASTExprBin(leftExpression, rightExpression) {}

string ASTExprMin::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"-\"]" + ";\n";
    return expr + ASTExprBin::getDotRepresentation();
}

string ASTExprMin::buildIR(CFG* cfg){
    string op1(leftExpression->buildIR(cfg));
    string op2(rightExpression->buildIR(cfg));
    BasicBlock *currentBB = cfg->getCurrentBB();
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op1);
    params.push_back(op2);

    currentBB->add_IRInstr(IRInstr::Operation::sub, SymbolType::INT, params);
    return resName;
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprAdd                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprAdd::ASTExprAdd(ASTExpr *leftExpression, ASTExpr *rightExpression) : ASTExprBin(leftExpression, rightExpression) {}

string ASTExprAdd::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"+\"]" + ";\n";
    return expr + ASTExprBin::getDotRepresentation();
}

string ASTExprAdd::buildIR(CFG* cfg){
    string op1(leftExpression->buildIR(cfg));
    string op2(rightExpression->buildIR(cfg));
    BasicBlock *currentBB = cfg->getCurrentBB();
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op1);
    params.push_back(op2);

    currentBB->add_IRInstr(IRInstr::Operation::add, SymbolType::INT, params);
    return resName;
}





//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprMod                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprMod::ASTExprMod(ASTExpr *leftExpression, ASTExpr *rightExpression) : ASTExprBin(leftExpression, rightExpression) {}

string ASTExprMod::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"%\"]" + ";\n";
    return expr + ASTExprBin::getDotRepresentation();
}

string ASTExprMod::buildIR(CFG* cfg){
    string op1(leftExpression->buildIR(cfg));
    string op2(rightExpression->buildIR(cfg));
    BasicBlock *currentBB = cfg->getCurrentBB();
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op1);
    params.push_back(op2);

    currentBB->add_IRInstr(IRInstr::Operation::mod, SymbolType::INT, params);
    return resName;
}