//
// Created by matmont98 on 01/04/2020.
//

#include <Logging.h>
#include "ast/AST.h"
#include "IR.h"

using namespace std;


//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTAffect                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTAffect::ASTAffect(ASTVar* variable, ASTExpr* expression) : variable(variable), expression(expression) {}

string ASTAffect::getDotRepresentation() {
    //cout << nodeID;
    string res(to_string(++nodeID) + "[label=\"=\"]" + ";\n");
    res += to_string(nodeID) + " -- " + to_string(nodeID + 1) + ";\n";
    res += to_string(nodeID) + " -- " + to_string(nodeID + 2) + ";\n";
    res += variable->getDotRepresentation();
    res += expression->getDotRepresentation();
    return res;
}

ASTVar *ASTAffect::getVariable() const {
    return variable;
}

void ASTAffect::process(VariableChecker& variableChecker) {
    variableChecker.setSymbolInit(variable->getName());

    if(variableChecker.isOnTheRight()){
        variable->process(variableChecker); // Set it on the right!
        expression->process(variableChecker); // Expression is on the right side, variables contained in it are on the right, propagate.
    }
    else{ // We are at the root of the tree
        LOG_TRACE << "Affecting  " + variable->getName();
        variableChecker.setOnTheRight(true);
        expression->process(variableChecker); // Expression is on the right side, variables contained in it are on the right, propagate.
        variableChecker.setOnTheRight(false);
    }
}

string ASTAffect::buildIR(CFG *cfg) {
    string tmpvar = expression->buildIR(cfg);
    vector<string> params = { "eax", tmpvar  };
    cfg->getCurrentBB()->add_IRInstr(IRInstr::Operation::copy, SymbolType::INT, params);
    params = { variable->getName(), "eax" };
    cfg->getCurrentBB()->add_IRInstr(IRInstr::Operation::copy, SymbolType::INT, params);
    return variable->getName();
}
