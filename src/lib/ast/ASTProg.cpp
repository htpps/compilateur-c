//
// Created by matmont98 on 01/04/2020.
//

// *** ASTProg ***

#include <utility>

#include "ast/AST.h"
#include "IR.h"
#include "Logging.h"

using namespace std;

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTProg                                                               //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

string ASTProg::getDotRepresentation() {
    string res(to_string(++nodeID) + " [label=\"ROOT\"]" + ";\n");

    int idParent = nodeID;

    for (auto &function : functions) {
        res += to_string(idParent) + " -- " + to_string(nodeID+1) + ";\n";
        res += function->getDotRepresentation();
    }

    res += to_string(idParent) + "--" + to_string(nodeID+1) + ";\n";
    res += to_string(++nodeID) + " [label=\"Func Main\"]" + ";\n";
    res += to_string(nodeID) + "--" + to_string(nodeID + 1) + ";\n";
    res += bloc->getDotRepresentation();
    return res;
}

void ASTProg::addFunction(ASTFunc * astFunc) {
    functions.push_back(astFunc);
}

int ASTProg::getVarNb() const {
    return varNb;
}

void ASTProg::setVarNb(int varNb) {
    ASTProg::varNb = varNb;
}

ASTBloc *ASTProg::getBloc() const {
    return bloc;
}

void ASTProg::setBloc(ASTBloc *bloc) {
    ASTProg::bloc = bloc;
}

void ASTProg::process(VariableChecker &variableChecker) {
    for(auto &it: functions){
        it->process(variableChecker);
    }

    variableChecker.openContext("main");

    bloc->process(variableChecker);

    variableChecker.closeContext();
}

string ASTProg::buildIR(CFG *cfg) {
    for(auto &it : functions){
        it->buildIR(cfg);
    }


    auto BBmain = new BasicBlock(cfg, "main");
    cfg->addBB(BBmain);
    cfg->setCurrentBB(BBmain);

    std::vector<string> params = {"main"};
    cfg->getSymbolTable()->openContext("main");
    cfg->getCurrentBB()->add_IRInstr(IRInstr::Operation::open_context, SymbolType::INT, params);

    bloc->buildIR(cfg);

    cfg->getCurrentBB()->add_IRInstr(IRInstr::Operation::close_context, SymbolType::INT, params);
    cfg->getSymbolTable()->closeContext();

    return "";
}