//
// Created by matmont98 on 01/04/2020.
//

#include <Logging.h>
#include "ast/AST.h"
#include "IR.h"

using namespace std;

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTDeclVar                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTDeclVar::ASTDeclVar(ASTVar *variable) : variable(variable) {}

string ASTDeclVar::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"DeclVar\"]" + ";\n";
    expr += to_string(nodeID) + " -- " + to_string(nodeID + 1) + ";\n";
    expr += variable->getDotRepresentation();
    return expr;
}

void ASTDeclVar::process(VariableChecker& variableChecker) {
    variableChecker.addSymbolToTable(new Symbol(variable->getName(), SymbolType::INT));
}

string ASTDeclVar::buildIR(CFG* cfg){
    return "";
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTDeclAffect                                                         //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTDeclAffect::ASTDeclAffect(ASTAffect *affectation) : affectation(affectation) {}

string ASTDeclAffect::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"DeclAffect\"]" + ";\n";
    expr += to_string(nodeID) + " -- " + to_string(nodeID + 1) + ";\n";
    expr += affectation->getDotRepresentation();
    return expr;
}

void ASTDeclAffect::process(VariableChecker& variableChecker) {
    variableChecker.addSymbolToTable(new Symbol(affectation->getVariable()->getName(), SymbolType::INT));
    affectation->process(variableChecker);
}


string ASTDeclAffect::buildIR(CFG *cfg) {
    return affectation->buildIR(cfg);
}