//
// Created by linux on 06/04/2020.
//

#include "ast/ASTFunc.h"
#include "IR.h"

#include <utility>
#include <Function.h>
#include <Logging.h>

using namespace std;


ASTFunc::ASTFunc(const string& _typeRet, string name, ASTBloc *astBloc) : name(std::move(name)), astBloc(astBloc) {
    this->typeRet = Symbol::SymbolStringToType(_typeRet);
}

std::string ASTFunc::getDotRepresentation() {

    string res = to_string(++nodeID) + " [label=\"Func "+ name +"\"]" + ";\n";
    int idParent = nodeID;

    for (auto &parameter : parameters) {
        res += to_string(idParent) + " -- " + to_string(nodeID + 1) + ";\n";
        res += parameter->getDotRepresentation();
    }
    res += to_string(idParent) + " -- " + to_string(nodeID + 1) + ";\n";
    res += astBloc->getDotRepresentation();
    return res;
}

void ASTFunc::addParameter(ASTVar * var) {
    parameters.push_back(var);
}

void ASTFunc::process(VariableChecker & variableChecker) {
    // Open context
    variableChecker.openContext(name);

    // Incoming variables (we assume) have a value and are init.
    std::vector<SymbolType> params;

    for(auto &it : parameters){
        auto *symbol = new Symbol(it->getName(), SymbolType::INT);
        params.push_back(symbol->getType());
        variableChecker.addSymbolToTable(symbol);
        variableChecker.setSymbolInit(it->getName());
    }

    // Create Function, add it to the FunctionTable with the variableChecker
    auto *function = new Function(typeRet, params, name);
    variableChecker.addFunctionToTable(function);

    // Process the code.
    astBloc->process(variableChecker);

    // Close context
    variableChecker.closeContext();
}

std::string ASTFunc::buildIR(CFG *cfg) {
    // Close every context, but save it
    //auto context = cfg->getSymbolTable()->getContext();
    cfg->getSymbolTable()->setContext({name});

    BasicBlock *BBfunc = new BasicBlock(cfg, name);
    cfg->addBB(BBfunc);
    cfg->setCurrentBB(BBfunc);

    vector<string> params;
    params.push_back(name);

    for(auto &it : parameters){
        params.push_back(it->getName());
    }

    cfg->getCurrentBB()->add_IRInstr(IRInstr::Operation::function, SymbolType::INT, params);

    astBloc->buildIR(cfg);
    cfg->getCurrentBB()->add_IRInstr(IRInstr::Operation::ret, SymbolType::INT, vector<string>());

    // Close context, set old one
    cfg->getSymbolTable()->closeContext();

    return "";
}


