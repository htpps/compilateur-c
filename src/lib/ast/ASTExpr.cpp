//
// Created by matmont98 on 01/04/2020.
//

// *** ASTExprConst ***

#include "ast/AST.h"
#include "IR.h"

using namespace std;

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprConst                                                          //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprConst::ASTExprConst(ASTConst *astConst) : astConst(astConst) {}

string ASTExprConst::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"ExprConst\"]" + ";\n";
    expr += to_string(nodeID) + " -- " + to_string(nodeID + 1) + ";\n";
    expr += astConst->getDotRepresentation();
    return expr;
}

void ASTExprConst::process(VariableChecker& variableChecker) {}

std::string ASTExprConst::buildIR(CFG *cfg) {
    const int value = astConst->getValue();
    vector<string> params;
    string tmpName(cfg->create_new_tempvar(SymbolType::INT));
    params.push_back(tmpName);
    params.push_back(to_string(value));
    cfg->getCurrentBB()->add_IRInstr(IRInstr::Operation::ldconst, SymbolType::INT, params);
    return tmpName;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprVar                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprVar::ASTExprVar(ASTVar *astVar) : astVar(astVar) {}

string ASTExprVar::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"ExprVar\"]" + ";\n";
    expr += to_string(nodeID) + " -- " + to_string(nodeID + 1) + ";\n";
    expr += astVar->getDotRepresentation();
    return expr;
}

void ASTExprVar::process(VariableChecker& variableChecker) {
    if(variableChecker.isOnTheRight()){
        astVar->process(variableChecker);
    }
}

std::string ASTExprVar::buildIR(CFG *cfg) {
    return astVar->getName();
}




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprAffect                                                         //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprAffect::ASTExprAffect(ASTAffect *astAffect) : astAffect(astAffect) {}

string ASTExprAffect::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"ExprAffect\"]" + ";\n";
    expr += to_string(nodeID) + " -- " + to_string(nodeID + 1) + ";\n";
    expr += astAffect->getDotRepresentation();
    return expr;
}

void ASTExprAffect::process(VariableChecker& variableChecker) {
    astAffect->process(variableChecker);
}

std::string ASTExprAffect::buildIR(CFG *cfg) {
    return astAffect->buildIR(cfg);
}