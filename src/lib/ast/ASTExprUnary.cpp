//
// Created by matmont98 on 01/04/2020.
//

#include "IR.h"
#include "Logging.h"
#include "ast/AST.h"

using namespace std;

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprUnary                                                          //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprUnary::ASTExprUnary(ASTExpr* expression) : expression(expression) {}

std::string ASTExprUnary::getDotRepresentation() {
    string expr = to_string(nodeID) + " -- " + to_string(nodeID + 1) + ";\n";
    expr += expression->getDotRepresentation();
    return expr;
}

void ASTExprUnary::process(VariableChecker& variableChecker) {  // propagate on right
    expression->process(variableChecker);
}

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprPar                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprPar::ASTExprPar(ASTExpr* expression) : ASTExprUnary(expression) {}

string ASTExprPar::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"( )\"]" + ";\n";
    return expr + ASTExprUnary::getDotRepresentation();
}

string ASTExprPar::buildIR(CFG* cfg) {
    return expression->buildIR(cfg);
}

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprNeg                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

ASTExprNeg::ASTExprNeg(ASTExpr* expression) : ASTExprUnary(expression) {}

std::string ASTExprNeg::getDotRepresentation() {
    string expr = to_string(++nodeID) + " [label=\"-\"];\n";
    return expr + ASTExprUnary::getDotRepresentation();
}

std::string ASTExprNeg::buildIR(CFG* cfg) {
    BasicBlock* currentBB = cfg->getCurrentBB();
    string op(expression->buildIR(cfg));
    string resName(cfg->create_new_tempvar(SymbolType::INT));

    vector<string> params;
    params.push_back(resName);
    params.push_back(op);

    currentBB->add_IRInstr(IRInstr::Operation::neg, SymbolType::INT, params);
    return resName;
}
