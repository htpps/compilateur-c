//
// Created by matmont98 on 01/04/2020.
//

#include <ast/ASTBloc.h>
#include <Logging.h>

#include "ast/AST.h"
#include "IR.h"

using namespace std;

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTBloc                                                               //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

static int blockContextCount = 0;

ASTBloc::ASTBloc() : ASTInstr(), context("block-" + to_string(blockContextCount++)) {}

string ASTBloc::getDotRepresentation() {
    string res(to_string(++nodeID) + " [label=\"BLOC\"]" + ";\n");
    int idParent = nodeID;
    for (auto &instruction : instructions) {
        res += to_string(idParent) + " -- " + to_string(nodeID + 1) + ";\n";
        res += instruction->getDotRepresentation();
    }
    return res;
}

void ASTBloc::addInstruction(ASTInstr *instruction) {
    instructions.push_back(instruction);
}

void ASTBloc::process(VariableChecker& variableChecker) {
    variableChecker.openContext(context);
    for (ASTInstr *astInstr : instructions) {
        astInstr->process(variableChecker);
    }
    variableChecker.closeContext();
}

string ASTBloc::buildIR(CFG *cfg) {

    cfg->getSymbolTable()->openContext(context);
    std::vector<string> params = {context};
    cfg->getCurrentBB()->add_IRInstr(IRInstr::Operation::open_context, SymbolType::INT, params);

    for (auto &instr: instructions) {
        instr->buildIR(cfg);
    }

    cfg->getCurrentBB()->add_IRInstr(IRInstr::Operation::close_context, SymbolType::INT, params);
    cfg->getSymbolTable()->closeContext();

    return "";
}
