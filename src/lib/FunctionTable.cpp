//
// Created by linux on 07/04/2020.
//

#include <Exceptions.h>
#include "FunctionTable.h"

void FunctionTable::addFunction(Function *function) {
    if(functions.count(function->getName())==0){
        functions.insert({function->getName(), function});
    } else{
        throw GenericException(ExceptionType::AlreadyExistFunctionException, "Redefinition of '"+ function->getName() + "'");
    }
}

void FunctionTable::doesFunctionExist(const std::string& name, int nbParams) {
    if(functions.count(name)!=1){
        throw GenericException(ExceptionTypeEnum::FunctionNotExistException, "Implicit declaration of function '"+ name +"'");
    }
    else{
        if(functions[name]->getNbParams() != 0 && functions[name]->getNbParams() != nbParams)
        {
            throw GenericException(ExceptionTypeEnum::FunctionExistWrongNbParamsException, "The function '" + name + "' exist but expects " + std::to_string(functions[name]->getNbParams()) + " parameters (" + std::to_string(nbParams) + " given)");
        }
    }
}
