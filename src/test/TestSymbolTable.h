//
// Created by mathieu on 18/03/2020.
//

#pragma once

#include "SymbolTable.h"
#include "Exceptions.h"
#include "Symbol.h"
#include "Logging.h"
#include <string>
#include <map>

struct SymbolTableTester {
    static std::vector<std::string> getContext(SymbolTable& st) {
        return st.context;
    }

    static std::map<std::string, Symbol*> getMap(SymbolTable& st) {
        return st.symbol_table;
    }

    static std::string getVectorToString(SymbolTable& st, const std::vector<std::string>& ctx) {
        return st.vectorToString(ctx);
    }

    static void showMap(SymbolTable& st) {
        for(auto const&[key, val] : st.symbol_table) {
            LOG_DEBUG << key << " : [" << *val << "]";
        }
    }
};

BOOST_AUTO_TEST_SUITE(testSymbolTable)

    BOOST_AUTO_TEST_CASE(openContextMain) {
        SymbolTable st;
        std::string context = "/main/";
        st.openContext("main");
        BOOST_CHECK_EQUAL(SymbolTableTester::getVectorToString(st, SymbolTableTester::getContext(st)), context);
    }

    BOOST_AUTO_TEST_CASE(openContextMainIf) {
        SymbolTable st;
        std::string context = "/main/if/";
        st.openContext("main");
        st.openContext("if");
        BOOST_CHECK_EQUAL(SymbolTableTester::getVectorToString(st, SymbolTableTester::getContext(st)), context);
    }

    BOOST_AUTO_TEST_CASE(closeContextMainIf) {
        SymbolTable st;
        std::string context = "/main/";
        st.openContext("main");
        st.openContext("if");
        st.closeContext();
        BOOST_CHECK_EQUAL(SymbolTableTester::getVectorToString(st, SymbolTableTester::getContext(st)), context);
    }

    BOOST_AUTO_TEST_CASE(closeContextMain) {
        SymbolTable st;
        std::string context = "/";
        st.openContext("main");
        st.openContext("if");
        st.closeContext();
        st.closeContext();
        BOOST_CHECK_EQUAL(SymbolTableTester::getVectorToString(st, SymbolTableTester::getContext(st)), context);
    }

    BOOST_AUTO_TEST_CASE(closeContextManyOperation) {
        SymbolTable st;
        std::string context = "/";
        st.openContext("main");
        st.openContext("if1");
        st.closeContext();
        st.openContext("else1");
        st.openContext("while1");
        st.closeContext();
        st.closeContext();
        st.closeContext();
        st.openContext("function1");
        st.openContext("while1");
        st.openContext("while2");
        st.closeContext();
        st.closeContext();
        st.closeContext();

        BOOST_CHECK_EQUAL(SymbolTableTester::getVectorToString(st, SymbolTableTester::getContext(st)), context);
    }

    BOOST_AUTO_TEST_CASE(closeContextClosedToMuch) {
        SymbolTable st;
        st.openContext("main");
        st.openContext("if");
        st.closeContext();
        st.closeContext();

        try{
            st.closeContext();
        }
        catch(GenericException e){
            BOOST_CHECK_EQUAL(e.getExceptionType(), ExceptionType::NoContextException);
        }
    }

    BOOST_AUTO_TEST_CASE(addSymbol) {
        SymbolTable st;
        auto* symbol = new Symbol("name", SymbolTypeEnum::INT, 1);
        st.openContext("main");
        st.addSymbol(symbol);
        st.closeContext();
        SymbolTableTester::showMap(st);
        BOOST_CHECK_EQUAL(*(SymbolTableTester::getMap(st)["/main/name"]), *symbol);
    }

    BOOST_AUTO_TEST_CASE(addSymbolSame) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        auto* symbol2 = new Symbol("name", SymbolTypeEnum::INT, 3);
        st.openContext("main");
        st.addSymbol(symbol1);

        try{
            st.addSymbol(symbol2);
        }

        catch(GenericException e){
            BOOST_CHECK_EQUAL(e.getExceptionType(), ExceptionType::AlreadyExistingSymbolException);
        }
        st.closeContext();
    }

    BOOST_AUTO_TEST_CASE(addSymbolSameNameContextDifferent) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        auto* symbol2 = new Symbol("name", SymbolTypeEnum::INT, 3);

        st.openContext("main");
        st.addSymbol(symbol1);
        st.openContext("if");
        st.addSymbol(symbol2);

        BOOST_CHECK_EQUAL(*(SymbolTableTester::getMap(st)["/main/name"]), *symbol1);
        BOOST_CHECK_EQUAL(*(SymbolTableTester::getMap(st)["/main/if/name"]), *symbol2);
    }

    BOOST_AUTO_TEST_CASE(addSymbolSameNameSameDifferent) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        auto* symbol2 = new Symbol("name", SymbolTypeEnum::INT, 3);

        st.openContext("main");
        st.addSymbol(symbol1);
        st.openContext("if");
        st.closeContext();

        try{
            st.addSymbol(symbol2);
        }
        catch(GenericException e){
            BOOST_CHECK_EQUAL(e.getExceptionType(), ExceptionType::AlreadyExistingSymbolException);
        }
    }

    BOOST_AUTO_TEST_CASE(getSymbol) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        st.openContext("main");
        st.addSymbol(symbol1);

        BOOST_CHECK_EQUAL(*(st.getSymbol("name")), *symbol1);
    }


    BOOST_AUTO_TEST_CASE(getSymbolUpperContext) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        st.openContext("main");
        st.addSymbol(symbol1);
        st.openContext("if");

        BOOST_CHECK_EQUAL(*(st.getSymbol("name")), *symbol1);
    }

    BOOST_AUTO_TEST_CASE(getSymbolNotInTable) {
        SymbolTable st;
        new Symbol("name", SymbolTypeEnum::INT, 1);

        try{
            st.getSymbol("name");
        }
        catch(GenericException e){
            BOOST_CHECK_EQUAL(e.getExceptionType(), ExceptionType::SymbolNotInTableException);
        }
    }

    BOOST_AUTO_TEST_CASE(getSymbolOtherContext) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        st.openContext("main");
        st.addSymbol(symbol1);
        st.closeContext();
        st.openContext("if");

        try{
            st.getSymbol("name");
        }
        catch(GenericException e){
            BOOST_CHECK_EQUAL(e.getExceptionType(), ExceptionType::SymbolNotInTableException);
        }
    }

    BOOST_AUTO_TEST_CASE(getSymbolComlexeContext) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        st.openContext("main");

        st.openContext("while");
        st.addSymbol(symbol1);
        st.closeContext();

        st.openContext("if1");
        st.openContext("if2");
        st.closeContext();
        st.openContext("if3");
        st.openContext("function");
        st.openContext("while");
        st.closeContext();

        try{
            st.getSymbol("name");
        }
        catch(GenericException e){
            BOOST_CHECK_EQUAL(e.getExceptionType(), ExceptionType::SymbolNotInTableException);
        }
    }

    BOOST_AUTO_TEST_CASE(getNumberVariables1Context_0VAR) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        st.openContext("main");
        st.closeContext();
        int nb = st.getMemorySpaceInContext("/main");

        BOOST_CHECK_EQUAL(nb,0);
    }

    BOOST_AUTO_TEST_CASE(getNumberVariables1Context_1VAR) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        st.openContext("main");
        st.addSymbol(symbol1);
        st.closeContext();
        int nb = st.getMemorySpaceInContext("/main");

        BOOST_CHECK_EQUAL(nb,4);
    }

    BOOST_AUTO_TEST_CASE(getNumberVariables1Context_2VAR) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        auto* symbol2 = new Symbol("name_2", SymbolTypeEnum::INT, 1);
        st.openContext("main");
        st.addSymbol(symbol1);
        st.addSymbol(symbol2);
        st.closeContext();
        int nb = st.getMemorySpaceInContext("/main");

        BOOST_CHECK_EQUAL(nb,2*4);
    }

    BOOST_AUTO_TEST_CASE(getNumberVariables2Context_2VAR) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        auto* symbol2 = new Symbol("name_2", SymbolTypeEnum::INT, 1);
        st.openContext("main");
        st.addSymbol(symbol1);
        st.openContext("while");
        st.addSymbol(symbol2);
        st.closeContext();
        st.closeContext();
        int nb = st.getMemorySpaceInContext("/main");

        BOOST_CHECK_EQUAL(nb,8);
    }

    BOOST_AUTO_TEST_CASE(getNumberVariables3Context_2VAR) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        auto* symbol2 = new Symbol("name_2", SymbolTypeEnum::INT, 1);
        st.openContext("main");
        st.openContext("if");
        st.addSymbol(symbol1);
        st.closeContext();
        st.openContext("else");
        st.addSymbol(symbol2);
        st.closeContext();
        st.closeContext();
        int nb = st.getMemorySpaceInContext("/main");

        BOOST_CHECK_EQUAL(nb,8);
    }

    BOOST_AUTO_TEST_CASE(getNumberVariables3Context_3VAR) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        auto* symbol2 = new Symbol("name_2", SymbolTypeEnum::INT, 1);
        auto* symbol0 = new Symbol("name_0", SymbolTypeEnum::INT, 1);
        st.openContext("main");
        st.addSymbol(symbol0);
        st.openContext("if");
        st.addSymbol(symbol1);
        st.closeContext();
        st.openContext("else");
        st.addSymbol(symbol2);
        st.closeContext();
        st.closeContext();
        int nb = st.getMemorySpaceInContext("/main");

        BOOST_CHECK_EQUAL(nb,12);
    }

    BOOST_AUTO_TEST_CASE(getNumberVariables3Context_3VAR_2) {
        SymbolTable st;
        auto* symbol1 = new Symbol("name", SymbolTypeEnum::INT, 1);
        auto* symbol2 = new Symbol("name_2", SymbolTypeEnum::INT, 1);
        auto* symbol0 = new Symbol("name_0", SymbolTypeEnum::INT, 1);
        st.openContext("main");
        st.addSymbol(symbol0);
        st.openContext("if");
        st.addSymbol(symbol1);
        st.closeContext();
        st.openContext("else");
        st.addSymbol(symbol2);
        st.closeContext();
        st.closeContext();
        int nb = st.getMemorySpaceInContext("/main/if");

        BOOST_CHECK_EQUAL(nb,4);
    }

BOOST_AUTO_TEST_SUITE_END()
