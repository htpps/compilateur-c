#include "cli.h"
#include "Logging.h"
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;
namespace po = boost::program_options;

po::variables_map parseCommandLine(int argc, const char** argv) {
    // parse cli options
    po::options_description cliOptions;
    po::options_description genericOptions("Allowed options");
    po::options_description hiddenOptions;

    po::positional_options_description posDesc;

    posDesc.add("input-file", 1);

    genericOptions.add_options()
        ("help,h", "display help message")
        ("verbose,v", po::bool_switch(), "enables debug output")
        ("output-file,o", po::value<string>()->default_value("a.s"), "output file where assembly will be generated")
        ("ast", po::value<string>()->implicit_value("ast.dot"), "generate a dot file (for use with graphviz) that represents the AST that was generated during program analysis (for debugging purposes)")
    ;

    hiddenOptions.add_options()
        ("input-file,i", po::value<string>(), "input file to compile")
    ;

    cliOptions.add(genericOptions).add(hiddenOptions);

    po::variables_map args;
    po::store(po::command_line_parser(argc, argv).options(cliOptions).positional(posDesc).run(), args);
    po::notify(args);


    if(args.count("help")) {
        cout << "Usage : " << argv[0] << " [options] <input-file>\n\n"
             << genericOptions << endl;
        exit(0);
    }

    if(!args.count("input-file")) {
        cout << "Usage : " << argv[0] << " [options] <input-file>\n"
             << "Run " << argv[0] << " --help to display help message" << endl;
        exit(0);
    }

    if(args["verbose"].as<bool>())
        setSeverityLevel(boost::log::trivial::trace);
    else
        setSeverityLevel(boost::log::trivial::info);

    LOG_TRACE << "Enabled verbose output";

    return args;
}
