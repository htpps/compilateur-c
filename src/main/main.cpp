#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <execinfo.h>
#include <csignal>
#include "VariableChecker.h"
#include "cli.h"
#include "antlr4-runtime.h"
#include "ifccLexer.h"
#include "ifccParser.h"
#include "Visitor.h"
#include "Logging.h"
#include "ast/AST.h"
#include "ast/ASTTreeRepr.h"
#include "SymbolTable.h"
#include "IR.h"

using namespace std;
using namespace antlr4;

void crashHandler(int sig) {
    constexpr int backtraceSize = 20;
    void *array[backtraceSize];
    size_t size;

    // get void*'s for all entries on the stack
    size = backtrace(array, backtraceSize);
    char** symbols = backtrace_symbols(array, size);

    // print out all the frames to stderr
    stringstream ss;
    ss << "Error: signal " << sig << '\n';

    for(int i = 0; i < backtraceSize; i++)
        ss << symbols[i] << '\n';

    LOG_FATAL << ss.str();

    exit(1);
}

int main(int argc, const char** argv) {
    signal(SIGSEGV, &crashHandler);
    signal(SIGABRT, &crashHandler);

    // Arguments parsing to get activated options
    auto args = parseCommandLine(argc, argv);


    // Read the file content
    stringstream in;
    ifstream lecture(args["input-file"].as<string>());
    in << lecture.rdbuf();

    ANTLRInputStream input(in.str());
    ifccLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    tokens.fill();

    /*for (auto token : tokens.getTokens()) {
        std::cout << token->toString() << std::endl;
    }*/

    // Token parsing according to our grammary
    ifccParser parser(&tokens);
    tree::ParseTree* tree = parser.axiom();

    if(parser.getNumberOfSyntaxErrors() != 0){
        LOG_ERROR << "Invalid syntax";
        exit(1);
    }

    Visitor visitor;
    ASTProg* astTree = visitor.visit(tree);

    // Génération du graphe au format DOT si option activée
    bool printGraph = args.count("ast");
    if(printGraph) {
        ofstream grapheTxt(args["ast"].as<string>());
        grapheTxt << getTreeRepresentation(astTree);
        grapheTxt.close();
        LOG_TRACE << "Generated AST dot file : " << args["ast"].as<string>();
    }


    // The symbol table with the memory address of every symbol
    SymbolTable symbolTable;
    FunctionTable functionTable;

    // Generate output file with the assembly code of the input file
    ofstream outputFile(args["output-file"].as<string>());
    LOG_TRACE << "Generating AST, aka futur process";
    //TODO ok pour l'instant, à voir pour faire plus propre et surtout bien intégrer aux tests


    VariableChecker variableChecker = VariableChecker(&symbolTable, &functionTable);

    try{
        astTree->process(variableChecker);
    }
    catch (GenericException& e){
        LOG_ERROR << e.getExceptionType() << e.what() << endl;
        return -1;
    }

    auto vectorSymbol = symbolTable.getUnusedSymbol();
    for(const auto it : vectorSymbol){
        LOG_WARNING << "Unused variable '" + it->getName() + "'";
    }

    auto errorList = variableChecker.getExceptions();
    for(const auto it : errorList){
        LOG_ERROR << "ERROR : " << it.what();
    }

    if(errorList.size() != 0){
        return 1;
    }
    else{
        LOG_TRACE << "Building IR";
        // IR
        //symbolTable.openContext("main");
        CFG cfg(astTree, &symbolTable);
        LOG_TRACE << "Writing assembly";
        cfg.gen_asm(outputFile);

        outputFile.close();

        LOG_TRACE << "Generated assembly output file : " << args["output-file"].as<string>();
        return 0;
    }
}
