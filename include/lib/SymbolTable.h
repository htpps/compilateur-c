//
// Created by hugnata on 3/11/20.
//

#pragma once

#include <map>
#include <string>
#include <vector>
#include "Symbol.h"

struct SymbolTableTester;

/**
 * Handles variables and addresses.
 */
class SymbolTable {

public:
    const std::string SEPARATOR = "/"; // Delimiter between 2 contexts used in vectorToString.
    const int BASE_ADDRESS = 0; // Base address given to a variable when entering a new function.

    SymbolTable();

    // Adds a symbol to the table and gives it an address.
    // throws UnsupportedTypeException, AlreadyExistingSymbolException
    void addSymbol(Symbol*);

    int getMemorySpaceInContext(std::string);

    // throws SymbolNotInTableException
    Symbol * getSymbol(const std::string&);

    // Set the symbol initialized
    void setSymbolInit(const std::string&);

    // Set the symbol used
    void setSymbolInTableUsed(const std::string&);

    // Opens a new context (new bloc, function etc).
    void openContext(const std::string&);

    // Closes a context.
    // throws NoContextException
    void closeContext();

    // Sets the context
    void setContext(std::vector<std::string>);

    // Dump the SymbolTable for debug
    void dump();

    // Get the unused symbols for warnings
    std::vector<Symbol*> getUnusedSymbol();

private:

    // Transform the vector context into a string (address of a symbol).
    std::string vectorToString(const std::vector<std::string>& ctx);

    // find a name in the symbol
    std::string findNameInTable(const std::string& var);

    // Acts as a queue, contains all the contexts.
    std::vector<std::string> context;

    // Contains the address and the symbol, <address/symbol_name, Symbol*>.
    std::map<std::string,Symbol*> symbol_table;

    // Contains the current address for a given function, incremented each time there is a new symbol added to the symbol_table by the amount of the size of the type of the Symbol.
    std::map<std::string,int> memoryAddress;

    // Map containing types (with the size) that are handled by our compiler.
    const std::map<SymbolTypeEnum, int> MapTypeSize = {{SymbolTypeEnum::INT, 4}}; // {{SymbolTypeEnum::INT, sizeof(int}}

    // Used to access private members for unit-testing.
    friend SymbolTableTester;
};
