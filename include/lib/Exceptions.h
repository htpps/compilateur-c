#pragma once

#include <exception>
#include <string>
#include <map>

/**
 * enumeration of possible types for exceptions
 */
enum ExceptionTypeEnum {
    SymbolNotInTableException,              // Raised when someone tries to access a Symbol not contained in the symbol_table.
    NoContextException,                     // Raised when someone tries to close the context that is already empty (if the context is "/" and someone tries to close it).
    AlreadyExistingSymbolException,         // Raised when someone tries to add a Symbol to the symbol_table that already exist (same name, same context).
    UnsupportedTypeException,               // Raised when someone tries to add a Symbol that has an unsupported type.
    UninitializedVariableException,         // Raised when someone tries to access the value of a variable that has not been initialized
    FunctionNotExistException,              // Raised when someone tries to access a function that does not exist
    AlreadyExistFunctionException,          // Raised when someone tries to add an already existing function to the FunctionTable
    IndexParamFunctionOutOfRangeException,  // Raised when someone tries to access the parameter at a wrong index of a funtion
    FunctionExistWrongNbParamsException,    // Raised when someone tries to call a function with the wrong number of parameters
    BasicBlockMissingTestVar,               //
};

typedef ExceptionTypeEnum ExceptionType;

class GenericException: public std::exception{
public:
    GenericException(ExceptionType exceptionType, std::string message);

    [[nodiscard]] const char* what() const noexcept override;
    [[nodiscard]] const ExceptionType& getExceptionType() const;

private:
    ExceptionType exceptionType;
    std::string message;
};