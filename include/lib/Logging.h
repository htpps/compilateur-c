#pragma once

#include <stdexcept>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/utility/manipulators/add_value.hpp>
#include <boost/log/trivial.hpp>

typedef boost::log::sources::severity_logger_mt<boost::log::trivial::severity_level> logger_t;

BOOST_LOG_GLOBAL_LOGGER(logger, logger_t)

// when defined, log records are colored with ansi escape sequences depending on severity
// undefine to disable colors
#define LOG_COLORS

#define LOG_TRACE   BOOST_LOG_SEV(logger::get(), boost::log::trivial::trace)

#define LOG_DEBUG   BOOST_LOG_SEV(logger::get(), boost::log::trivial::debug)

#define LOG_INFO    BOOST_LOG_SEV(logger::get(), boost::log::trivial::info)

#define LOG_WARNING BOOST_LOG_SEV(logger::get(), boost::log::trivial::warning)\
    << boost::log::add_value("File", __FILE__) << boost::log::add_value("Line", __LINE__)

#define LOG_ERROR   BOOST_LOG_SEV(logger::get(), boost::log::trivial::error)\
    << boost::log::add_value("File", __FILE__) << boost::log::add_value("Line", __LINE__)

#define LOG_FATAL   BOOST_LOG_SEV(logger::get(), boost::log::trivial::fatal)\
    << boost::log::add_value("File", __FILE__) << boost::log::add_value("Line", __LINE__)

void setSeverityLevel(boost::log::trivial::severity_level severityLevel);
