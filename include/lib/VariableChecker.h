//
// Created by matmont98 on 05/04/2020.
//

#include "SymbolTable.h"
#include "Exceptions.h"
#include "FunctionTable.h"
#include <list>

#pragma once

/*
 * Is used in the AST to check that variables exists, are initialised when it's value is accessed etc.
 * It is also used for functions, so VariableChecker should be renamed to StructureChecker
 * It is the bridge between the AST and the SymbolTable / FunctionTable. It catches all errors that are raised and
 * stores them into a vector so that main can display all the messages at the end of the execution.
 */
class VariableChecker {
public:
    VariableChecker(SymbolTable *, FunctionTable*);

    // Set its own state to the right (to now if a variable is used, ie a = b = c = d = e; b, c, d, e are used.
    void setOnTheRight(bool);
    // Returns onTheRight
    bool isOnTheRight();

    // Opens a new context
    void openContext(const std::string&);
    // Closes the last context
    void closeContext();

    // Adds a symbol to the SymbolTable.
    void addSymbolToTable(Symbol*);

    // Adds a function to the FunctionTable
    void addFunctionToTable(Function *function);
    // Check if the give function (name, number of parameters) exists
    void doesFunctionExist(const std::string&, int);

    // Set the symbol (name) to used
    void setSymbolInTableUsed(const std::string&);
    // Set the symbol (name) to initialized
    void setSymbolInit(const std::string&);

    // Returns all raised exceptions.
    std::list<GenericException> getExceptions();

private:

    SymbolTable *symbolTable;

    FunctionTable *functionTable;

    bool onTheRight;

    // List of all raised exceptions: if there is an error, the program does not stop and continues.
    std::list<GenericException> exceptions;
};

