//
// Created by matmont98 on 07/04/2020.
//

#pragma once

#include <vector>
#include "Symbol.h"

/*
 * Represent a function. Some features are implemented for futures updates
 * (here it supports all SymbolType, int is not hard coded)
 */
class Function {
public:
    Function(SymbolType typeRet, std::vector<SymbolType> typeParams, const std::string &name);

    // Check the equality between two functions
    bool operator==(const Function &rhs) const;

    // Returns the name of the function
    const std::string &getName() const;

    // Returns the number of parameters of the function
    int getNbParams() const;

    // Returns the parameters at the given index of the function
    SymbolType getParam(int);

private:
    // The return type of the function
    SymbolType typeRet;

    // The types of the parameters of the function
    std::vector<SymbolType> typeParams;

    // The name of the function
    std::string name;
};