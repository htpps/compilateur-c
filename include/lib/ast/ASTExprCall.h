//
// Created by soplump on 06/04/2020.
//

#pragma once

#include <ast/ASTExpr.h>

class ASTExprCall : ASTExpr {
public:
    ASTExprCall(std::string);

    void process(VariableChecker&) override;

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;

    void addAstExpression(ASTExpr*);

protected:
    std::string label;
    std::vector<ASTExpr*> expressions;
};