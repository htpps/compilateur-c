//
// Created by matmont98 on 01/04/2020.
//

#pragma once

#include "ASTNode.h"
#include "ASTTreeRepr.h"

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTVar                                                                //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to a variable
 */
class ASTVar : public ASTNode {
public:
    explicit ASTVar(std::string name) ;

    std::string getDotRepresentation() override;

    const std::string& getName() const;

    void process(VariableChecker&) override ;

    std::string buildIR(CFG* cfg) override;

private:
    std::string name;
    bool onTheRight;
};