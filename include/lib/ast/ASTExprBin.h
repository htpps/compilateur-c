//
// Created by matmont98 on 01/04/2020.
//

#pragma once

#include "ASTNode.h"
#include "ASTExpr.h"
#include "ASTTreeRepr.h"

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprBin                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprBin : public ASTExpr {

public:

    ASTExprBin(ASTExpr* leftExpression, ASTExpr* rightExpression);

    std::string getDotRepresentation() override;

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override = 0;


protected:
    ASTExpr* leftExpression;
    ASTExpr* rightExpression;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprAdd                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprAdd : public ASTExprBin {

public:

    ASTExprAdd(ASTExpr *leftExpression, ASTExpr *rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;

private:
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprDiv                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprDiv : public ASTExprBin {

public:

    ASTExprDiv(ASTExpr *leftExpression, ASTExpr *rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;

private:
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprMin                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprMin : public ASTExprBin {

public:

    ASTExprMin(ASTExpr *leftExpression, ASTExpr *rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;

private:
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprMod                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprMod : public ASTExprBin {

public:

    ASTExprMod(ASTExpr *leftExpression, ASTExpr *rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;

private:
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprMul                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprMul : public ASTExprBin {

public:

    ASTExprMul(ASTExpr *leftExpression, ASTExpr *rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;

private:
};