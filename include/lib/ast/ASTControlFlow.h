#pragma once

#include "ASTInstr.h"

class ASTExpr;
class ASTInstr;

static int controlFlowNb = 0;
//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTControlIf                                                          //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTControlIf : public ASTInstr {
public:
    ASTControlIf(ASTExpr* predicate, ASTInstr* ifInstr, ASTInstr* elseInstr);
    std::string getDotRepresentation() override;
    void process(VariableChecker& checker) override;
    std::string buildIR(CFG* cfg) override;
protected:
    ASTExpr* predicate;
    ASTInstr* ifInstr;
    ASTInstr* elseInstr;
};


//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTControlWhile                                                       //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTControlWhile : public ASTInstr {
public:
    ASTControlWhile(ASTExpr* predicate, ASTInstr* instr);
    std::string getDotRepresentation() override;
    void process(VariableChecker& checker) override;
    std::string buildIR(CFG* cfg) override;
protected:
    ASTExpr* predicate;
    ASTInstr* instr;
};


//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTControlDoWhile                                                     //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTControlDoWhile : public ASTInstr {
public:
    ASTControlDoWhile(ASTExpr* predicate, ASTInstr* instr);
    std::string getDotRepresentation() override;
    void process(VariableChecker& checker) override;
    std::string buildIR(CFG* cfg) override;
protected:
    ASTExpr* predicate;
    ASTInstr* instr;
};