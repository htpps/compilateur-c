//
// Created by matmont98 on 01/04/2020.
//

#pragma once

#include "ASTNode.h"
#include "ASTTreeRepr.h"

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExpr                                                               //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTAffect;
class ASTConst;
class ASTVar;

/**
 * Call to an expression.
 */
class ASTExpr : public ASTNode {
public:

    void process(VariableChecker&) override = 0;

    std::string getDotRepresentation() override = 0;

    std::string buildIR(CFG* cfg) override = 0;

private:
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprVar                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to a expression which is a variable.
 */
class ASTExprVar : public ASTExpr {
public:
    explicit ASTExprVar(ASTVar* astVar);

    std::string getDotRepresentation() override ;

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override;

private:
    ASTVar* astVar;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprAffect                                                         //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to a expression which is an affectation.
 */
class ASTExprAffect : public ASTExpr {

public:
    explicit ASTExprAffect(ASTAffect* astAffect);

    std::string getDotRepresentation() override;

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override ;

private:
    ASTAffect* astAffect;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprConst                                                          //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to a expression which is a constant.
 */
class ASTExprConst : public ASTExpr {
public:
    explicit ASTExprConst(ASTConst* astConst);

    std::string getDotRepresentation() override;

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override;

private:
    ASTConst* astConst;
};

