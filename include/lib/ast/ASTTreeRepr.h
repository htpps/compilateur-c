//
// Created by linux on 01/04/2020.
//

#pragma once
#include "ASTNode.h"

extern int nodeID;  // Compteur de noeuds créés pour le graphe

std::string getTreeRepresentation(ASTNode *rootNode);