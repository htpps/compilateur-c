//
// Created by matmont98 on 01/04/2020.
//

#pragma once

#include "ASTNode.h"
#include "ASTTreeRepr.h"
#include <list>

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTInstr                                                              //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExpr;
class ASTDecl;

/**
 * Call to an abstract instruction
 */
class ASTInstr : public ASTNode {
public:
    void process(VariableChecker&) override = 0;
    std::string getDotRepresentation() override = 0;
    std::string buildIR(CFG* cfg) override = 0;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTInstrDecl                                                          //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to a declaration which is a instruction.
 */
class ASTInstrDecl : public ASTInstr {
public:
    ASTInstrDecl() = default;

    std::string getDotRepresentation() override;

    void addAstDeclaration(ASTDecl *);

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override;

private:
    std::list<ASTDecl*> declarations;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTInstrDecl                                                          //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to a expression which is an instruction.
 */
class ASTInstrExpr : public ASTInstr {
public:
    explicit ASTInstrExpr(ASTExpr* expression);

    std::string getDotRepresentation() override;

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override;

private:
    ASTExpr* expression;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTInstrReturn                                                        //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to an instruction which is a return instruction.
 */
class ASTInstrReturn : public ASTInstr {
public:
    explicit ASTInstrReturn(ASTExpr* expression);

    std::string getDotRepresentation() override;

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override;

private:
    ASTExpr* expression;
};


