#pragma once

#include "ASTNode.h"
#include "ASTExprBin.h"
#include "ASTTreeRepr.h"
//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprLE                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprLE : public ASTExprBin {
public:
    ASTExprLE(ASTExpr* leftExpression, ASTExpr* rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprLT                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprLT : public ASTExprBin {
public:
    ASTExprLT(ASTExpr* leftExpression, ASTExpr* rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprGE                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprGE : public ASTExprBin {
public:
    ASTExprGE(ASTExpr* leftExpression, ASTExpr* rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprGT                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprGT : public ASTExprBin {
public:
    ASTExprGT(ASTExpr* leftExpression, ASTExpr* rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprEq                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprEq : public ASTExprBin {
public:
    ASTExprEq(ASTExpr* leftExpression, ASTExpr* rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprNE                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprNE : public ASTExprBin {
public:
    ASTExprNE(ASTExpr* leftExpression, ASTExpr* rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprAnd                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprAnd : public ASTExprBin {
public:
    ASTExprAnd(ASTExpr* leftExpression, ASTExpr* rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprOr                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprOr : public ASTExprBin {
public:
    ASTExprOr(ASTExpr* leftExpression, ASTExpr* rightExpression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;
};
