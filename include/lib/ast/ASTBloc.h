//
// Created by matmont98 on 01/04/2020.
//

#pragma once

#include <list>

#include "ASTNode.h"
#include "ASTTreeRepr.h"
#include "ASTInstr.h"

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                                ASTBloc                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to a bloc of code
 */
class ASTBloc : public ASTInstr {
public:
    ASTBloc();

    void addInstruction(ASTInstr* instruction);

    std::string getDotRepresentation() override;

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override;

private:
    std::string context;
    std::list<ASTInstr *> instructions; // TODO Switch to vector
};
