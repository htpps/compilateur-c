//
// Created by matmont98 on 01/04/2020.
//

#pragma once
#include "SymbolTable.h"
#include "VariableChecker.h"

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTNode                                                               //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class IRInstr;
class CFG;

/**
 * ASTNode is the main node of the AST.
 * Abstract class. Mother of ASTVar, ASTConst, ASTAffect, ASTExpr, ASTDecl, ASTInstr, ASTBloc and ASTProg.
 */
class ASTNode {
public:
    /** Generate the dot representation of the complete AST **/
    virtual std::string getDotRepresentation() = 0;

    /** Add variables to the symbolTable **/
    virtual void process(VariableChecker&) = 0;

    virtual std::string buildIR(CFG* cfg) = 0;
};
