//
// Created by matmont98 on 06/04/2020.
//
#pragma once

#include "AST.h"

class ASTFunc : public ASTNode  {
public:
    ASTFunc(const std::string& typeRet, std::string name, ASTBloc *astBloc);

    std::string getDotRepresentation() override;

    void addParameter(ASTVar*);

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override;

private:
    std::vector<ASTVar *> parameters;

    ASTBloc *astBloc;

    std::string name;

    SymbolType typeRet;
};
