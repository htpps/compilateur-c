//
// Created by matmont98 on 01/04/2020.
//

#pragma once

#include "ASTNode.h"
#include "ASTTreeRepr.h"

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTProg                                                               //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTBloc;
class ASTFunc;

/**
 * Call to the root node
 */
class ASTProg : public ASTNode {
public:
    int getVarNb() const;

    void setVarNb(int varNb);

    ASTBloc* getBloc() const;

    std::string getDotRepresentation() override;

    void addFunction(ASTFunc*);

    void setBloc(ASTBloc* bloc);

    void process(VariableChecker&) override;

    // Renvoie le nom de la variable temporaire de l"IR qui contient le résultat de l'évaluation
    std::string buildIR(CFG* cfg) override;

private:
    int varNb;
    ASTBloc* bloc;

    std::vector<ASTFunc *> functions;
};
