//
// Created by linux on 01/04/2020.
//

#pragma once

#include "ASTAffect.h"
#include "ASTBloc.h"
#include "ASTBooleanOperators.h"
#include "ASTControlFlow.h"
#include "ASTConst.h"
#include "ASTDecl.h"
#include "ASTExpr.h"
#include "ASTExprBin.h"
#include "ASTExprUnary.h"
#include "ASTFunc.h"
#include "ASTInstr.h"
#include "ASTNode.h"
#include "ASTProg.h"
#include "ASTVar.h"
#include "ASTExprCall.h"
