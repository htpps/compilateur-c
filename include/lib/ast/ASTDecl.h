//
// Created by matmont98 on 01/04/2020.
//

#pragma once

#include "ASTNode.h"
#include "ASTTreeRepr.h"

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTAffect                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTAffect;

/**
 * Call to a declaration (Abstract class)
 */
class ASTDecl : public ASTNode {
public:
    std::string getDotRepresentation() override = 0;
    void process(VariableChecker&) override = 0;
    std::string buildIR(CFG* cfg) override = 0;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTDeclAffect                                                         //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to a declaration which is an affectation.
 */
class ASTDeclAffect : public ASTDecl {
public:
    explicit ASTDeclAffect(ASTAffect* affectation);

    std::string getDotRepresentation() override;

    void process(VariableChecker&) override;

    std::string buildIR(CFG *cfg) override;

private:
    ASTAffect* affectation;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTDeclVar                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to a declaration which is a constant.
 */
class ASTDeclVar : public ASTDecl {
public:
    explicit ASTDeclVar(ASTVar* variable);

    std::string getDotRepresentation() override;

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override;

private:
    ASTVar* variable;
};
