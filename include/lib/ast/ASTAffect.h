//
// Created by matmont98 on 01/04/2020.
//

#pragma once

#include "ASTNode.h"
#include "ASTTreeRepr.h"

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTAffect                                                             //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to an affectation
 */
class ASTVar;
class ASTExpr;

class ASTAffect : public ASTNode {
public:
    ASTAffect(ASTVar* variable, ASTExpr* expression);

    std::string getDotRepresentation() override;

    ASTVar *getVariable() const;

    void process(VariableChecker&) override;

    std::string buildIR(CFG *cfg) override;

private:
    ASTVar* variable; // left-value of the affectation
    ASTExpr* expression; // right-value of the affectation
};