//
// Created by matmont98 on 01/04/2020.
//

#pragma once

#include "ASTNode.h"
#include "ASTTreeRepr.h"

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTConst                                                              //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

/**
 * Call to a constant
 */
class ASTConst : public ASTNode {
public:
    explicit ASTConst(int value);

    std::string getDotRepresentation() override;

    int getValue() const;

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override;

private:
    int value;
};