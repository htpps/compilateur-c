//
// Created by matmont98 on 01/04/2020.
//

#pragma once

#include "ASTNode.h"
#include "ASTExpr.h"
#include "ASTTreeRepr.h"

//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprUnary                                                          //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprUnary : public ASTExpr {

public:

    explicit ASTExprUnary(ASTExpr *expression);

    std::string getDotRepresentation() override;

    void process(VariableChecker&) override;

    std::string buildIR(CFG* cfg) override = 0;

private:

protected:
    ASTExpr* expression;
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprPar                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprPar : public ASTExprUnary {

public:
    ASTExprPar(ASTExpr *expression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;

private:
};




//--------------------------------------------------------------------------------------------------------------------//
//                                                                                                                    //
//                                              ASTExprPar                                                            //
//                                                                                                                    //
//--------------------------------------------------------------------------------------------------------------------//

class ASTExprNeg : public ASTExprUnary {
public:
    ASTExprNeg(ASTExpr* expression);

    std::string getDotRepresentation() override;

    std::string buildIR(CFG* cfg) override;
};