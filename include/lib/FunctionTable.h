//
// Created by matmont98 on 07/04/2020.
//

#pragma once


#include <string>
#include <map>
#include "Function.h"
/*
 * Same as the SymbolTable, but for functions: it is used to detect calls to undeclared functions.
 */
class FunctionTable {
public:
    // Add a function to the table
    void addFunction(Function *);

    // Check if the function exists (check the name and the numbers of parameters
    void doesFunctionExist(const std::string&, int);
private:
    std::map<std::string, Function* > functions;
};


