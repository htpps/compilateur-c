#pragma once

#include "antlr4-runtime.h"
#include "ifccBaseVisitor.h"
#include "ast/AST.h"

#include <list>
#include <Logging.h>

/**
 * This class provides an empty implementation of ifccVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class Visitor : public ifccBaseVisitor {
public:
    antlrcpp::Any visitAxiom(ifccParser::AxiomContext *ctx) override;

    antlrcpp::Any visitProg(ifccParser::ProgContext *ctx) override;

    antlrcpp::Any visitBloc(ifccParser::BlocContext *ctx) override;

    antlrcpp::Any visitInstExpr(ifccParser::InstExprContext *ctx) override;

    antlrcpp::Any visitInstDecl(ifccParser::InstDeclContext *ctx) override;

    antlrcpp::Any visitInstReturn(ifccParser::InstReturnContext *ctx) override;

    antlrcpp::Any visitExprCall(ifccParser::ExprCallContext *context) override;

    antlrcpp::Any visitExprConst(ifccParser::ExprConstContext *ctx) override;

    antlrcpp::Any visitExprVar(ifccParser::ExprVarContext *ctx) override;

    antlrcpp::Any visitExprAffect(ifccParser::ExprAffectContext *ctx) override;

    antlrcpp::Any visitAffect(ifccParser::AffectContext *ctx) override;

    antlrcpp::Any visitDeclVar(ifccParser::DeclVarContext *ctx) override;

    antlrcpp::Any visitDeclAffect(ifccParser::DeclAffectContext *ctx) override;

    antlrcpp::Any visitExprMul(ifccParser::ExprMulContext *context) override;

    antlrcpp::Any visitExprPar(ifccParser::ExprParContext *context) override;

    antlrcpp::Any visitExprAdd(ifccParser::ExprAddContext *context) override;

    antlrcpp::Any visitExprCmp(ifccParser::ExprCmpContext *context) override;

    antlrcpp::Any visitExprEq(ifccParser::ExprEqContext *context) override;

    antlrcpp::Any visitExprAnd(ifccParser::ExprAndContext *context) override;

    antlrcpp::Any visitExprOr(ifccParser::ExprOrContext *context) override;

    antlrcpp::Any visitFunc(ifccParser::FuncContext *context) override;

    antlrcpp::Any visitParameter(ifccParser::ParameterContext *context) override;

    antlrcpp::Any visitInstBloc(ifccParser::InstBlocContext* ctx) override;

    antlrcpp::Any visitIf(ifccParser::IfContext* ctx) override;

    antlrcpp::Any visitWhile(ifccParser::WhileContext* ctx) override;

    antlrcpp::Any visitFor(ifccParser::ForContext* ctx) override;

    antlrcpp::Any visitDoWhile(ifccParser::DoWhileContext* ctx) override;

    antlrcpp::Any visitExprNeg(ifccParser::ExprNegContext* ctx) override;
};
