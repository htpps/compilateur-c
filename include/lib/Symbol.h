#pragma once

#include <string>
#include <utility>
#include <ostream>

/**
 * enumeration of possible types for the variables
 */
enum SymbolTypeEnum {
    INT
};

typedef SymbolTypeEnum SymbolType;

/**
 * This class represent a variable
 */
class Symbol {
    public:
    Symbol(std::string name, SymbolType type, int address);
    Symbol(std::string name, SymbolType type);

    bool operator==(const Symbol &rhs) const;

    bool operator!=(const Symbol &rhs) const;

    const std::string& getName() const;

    int getAddress() const;

    SymbolType getType() const;

    void setAddress(int address);

    static std::string SymbolTypeToString(SymbolType);

    static SymbolType SymbolStringToType(const std::string& symbolString);

    friend std::ostream &operator<<(std::ostream &os, const Symbol &symbol);

    bool isUsed() const;

    void setUsed(bool used);

    bool isInitialized() const;

    void setInitialized(bool initialized);

private:
    /**
     * The name of the variable
     */
    std::string name;

    /**
     * Type of the variable
     */
    SymbolType type;

    /**
     * Address of the variable on the stack
     */
    int address;

    bool used = false;

    bool initialized = false;
};
