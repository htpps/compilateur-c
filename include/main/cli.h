#pragma once
#include <boost/program_options.hpp>

boost::program_options::variables_map parseCommandLine(int argc, const char** argv);
